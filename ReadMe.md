This folder contains all the code needed to run the patient specific source localization analysis for Thio et al. 2024 (Stereo-EEG Propagating Source Reconstruction Identifies New Surgical Targets for Epilepsy Patients).

All the code is in Code.

Code and data for each figure are found in the Figure folders.

## License
The copyrights of this software are owned by Duke University. As such, two licenses to this software are offered:
1. An open-source license under the GPLv2 license for non-commercial use.
2. A custom license with Duke University, for commercial use without the GPLv2 license restrictions. 
 
As a recipient of this software, you may choose which license to receive the code under. Outside contributions to the Duke-owned code base cannot be accepted unless the contributor transfers the copyright to those changes over to Duke University.
To enter a custom license agreement without the GPLv2  license restrictions, please contact the Duke Office for Translation & Commercialization (OTC) (https://olv.duke.edu/) at otcquestions@duke.edu with
reference to “OTC File No. 7973 in your email. 
 
Please note that this software is distributed AS IS, WITHOUT ANY WARRANTY; and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
