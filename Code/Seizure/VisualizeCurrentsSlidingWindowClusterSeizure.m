function VisualizeCurrentsSlidingWindowClusterSeizure(ind)
numSpikes=[4 4 4 2 3 12 8 6 5 5 3 6];%[4 6 6 2 6 12 8 6 5 5 3 6];
samplingFreqs=[2048 2048 2048 2048 2048 2048 2048 2048 1024 2048 2048 2048];
patients=[2 3 7 19 20 24 25 26 27 30 31 33];
ct=1;
seizureNums=cell(12,1);
seizureNums{1}=1:4;
seizureNums{2}=1:4;
seizureNums{3}=[1 3 7 11];
seizureNums{4}=[1 2];
seizureNums{5}=[1 2 8];
seizureNums{6}=1:12;
seizureNums{7}=1:8;
seizureNums{8}=1:6;
seizureNums{9}=1:5;
seizureNums{10}=[2 4 6 8 10];
seizureNums{11}=1:3;
seizureNums{12}=[1 3 4 5 20 21];
for i=1:length(patients)
    for j=1:numSpikes(i)
        if ct==ind
            patientNum=i;
            spikenum=j;
        end
        ct=ct+1;
    end
end
patient=['Patient' num2str(patients(patientNum))];
if ~isfile([patient '/Videos/IterativeExpansionSeizureWindowedOrganized_' num2str(seizureNums{patientNum}(spikenum)) '_smooth_2s_6view2_ISO1.avi'])
    views=[1 0 1 1 1 0 1 1 1 1 0 0];% 1 is Left 0 is right
    
    load([patient '/Gradients2.mat'])%V
    
    load([patient '/work/SmoothedBrainMesh2.mat'])
    TRI=scirunfield.face';
    Vertice_Location=scirunfield.node;
    elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
    V2=[];
    temp=load([patient '/Gradients.mat']);%V
    V2=temp.V;
    load([patient '/work/SmoothedBrainMesh.mat'])
    TRI2=scirunfield.face';
    Vertice_Location2=scirunfield.node;
    elemLoc2=(Vertice_Location2(:,TRI2(:,1))+Vertice_Location2(:,TRI2(:,2))+Vertice_Location2(:,TRI2(:,3)))./3;
    
    % graph1=CreateElementGraph(V,elemLoc);
    % graph2=CreateElementGraph(V2,elemLoc2);
    
    [inds] = knnsearch(elemLoc2',elemLoc','K',1,'Distance','euclidean');
    
    
    transfer=sparse(inds,1:length(TRI),ones(length(TRI),1),length(TRI2),length(TRI));
    
    % load(['Data\Patient' num2str(patients(patientNum)) '\Metrics.mat'])
    % mean(jaccard,3)
    FV=stlread([patient '/work/lh_brain_final2.stl']);
    LnumNodes=length(FV.Points);
    
    
    load([patient '/work/OmitCorticalPoints.mat'])
    if isfile('work/OmitNonCortex.mat')
        load([patient '/work/OmitNonCortex.mat'])
        omitinds=[omitinds omits];
    end
    elemLoc2(:,omitinds)=[];
    V2(:,omitinds)=[];
    V2(sum(V2,2)~=0,:)=[];
    transfer(omitinds,:)=[];
    load([patient '/work/OmitCorticalPoints2.mat'])
    if isfile('work/OmitNonCortex2.mat')
        load([patient '/work/OmitNonCortex2.mat'])
        omitinds=[omitinds omits];
    end
    LnumElems=length(FV.ConnectivityList)-length(find(omitinds<=length(FV.ConnectivityList)));
    elemLoc(:,omitinds)=[];
    V(:,omitinds)=[];
    V(sum(V,2)~=0,:)=[];
    graph1=CreateElementGraph(V,elemLoc);
    graph2=CreateElementGraph(V2,elemLoc2);
    transfer(:,omitinds)=[];
    TRI(omitinds,:)=[];
    % mean(dist,3)
    VertElemMap=sparse([1:length(TRI) 1:length(TRI) 1:length(TRI)],[TRI(:,1)' TRI(:,2)' TRI(:,3)'],1,length(TRI),length(Vertice_Location));

    numCPUs = feature('numCores'); % Get number of CPUs available
    pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function
    mkdir(pc_storage_dir);
    pc = parcluster('local');
    pc.JobStorageLocation = pc_storage_dir;
    fprintf('Number of CPUs requested = %g\n',numCPUs);
    poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead
    graph1=parallel.pool.Constant(graph1);
    elemLoc=parallel.pool.Constant(elemLoc);
    TRI=parallel.pool.Constant(TRI);
    Vertice_Location=parallel.pool.Constant(Vertice_Location);
    views=parallel.pool.Constant(views);
    samplingFreqs=parallel.pool.Constant(samplingFreqs);
    VertElemMap=parallel.pool.Constant(VertElemMap);
    name = [patient '/Ephys/SeizureTimeCourse_' num2str(seizureNums{patientNum}(spikenum)) '_1000.mat'];
    if isfile(name)
        load(name)
        load([patient '/Recons/J_sol_Ephys_Seizure_' num2str(seizureNums{patientNum}(spikenum)) '_1000.mat'])
        J_sol=parallel.pool.Constant(J_sol);
        filteredData=parallel.pool.Constant(filteredData);
        %% Optimize over time
        % Visualize
        
        allVerts=cell(samplingFreqs.Value(patientNum),1);
        parfor times=1:floor(length(J_sol.Value)/50)
            verts=[];
            for time=((1:50)+(times-1)*50)
                if time<length(J_sol.Value)
                    if ~isempty(J_sol.Value{time})
                        for i=1:length(J_sol.Value{time}(:,1))
                            verts=[verts makePatchOnly(J_sol.Value{time}(i,2),graph1.Value,J_sol.Value{time}(i,1))];
                        end
                    end
                end
            end
            allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc.Value(1,:)));
            %                 allVerts{times}=verts;
        end
        save([patient '/Recons/AllVerts20HzSampling_' num2str(seizureNums{patientNum}(spikenum)) '.mat'],'allVerts','-v7.3');
        allVerts=parallel.pool.Constant(allVerts);
        onTimes=length(J_sol.Value);
%         animation_frames=cell(floor((onTimes-400)/10),1);
        animation_frames=cell(1,1);
        
%         parfor timeCt=1:floor((onTimes-400)/20)
%             times=20+timeCt*2;
%             %                 verts=horzcat(allVerts{times:(times+99)});
%             %                 data=zeros(1,length(elemLoc(1,:)));
%             %                 for i=unique(verts)
%             %                     data(i)=length(find(verts==i));
%             %                 end
%             data=sum(vertcat(allVerts.Value{(times-20):(times+20)}));
%             data(data<10)=0;
% %             plotBrainSource2(full(data),TRI.Value,Vertice_Location.Value,views.Value(patientNum),filteredData.Value,(times*10)/samplingFreqs.Value(patientNum)*10,4000/samplingFreqs.Value(patientNum),length(filteredData.Value)/samplingFreqs.Value(patientNum)*10-30,30*samplingFreqs.Value(patientNum)/10+1)
%             plotBrainSource3(full(data),TRI.Value,Vertice_Location.Value,views.Value(patientNum),filteredData.Value,(times*10)/samplingFreqs.Value(patientNum)*10,4000/samplingFreqs.Value(patientNum),length(filteredData.Value)/samplingFreqs.Value(patientNum)*10-30,30*samplingFreqs.Value(patientNum)/10+1,VertElemMap.Value)
%             animation_frames{timeCt} = getframe(gcf);
%         end
%         v = VideoWriter([patient '/Videos/IterativeExpansionSeizureWindowedOrganized_' num2str(seizureNums{patientNum}(spikenum)) '_smooth_2s_6view.avi']);
%         v.FrameRate = 60;
%         open(v)
%         for frame_idx = 1:length(animation_frames)
%             writeVideo(v,animation_frames{frame_idx});
%         end
%         close(v)
    end
end