function [bestErr,bestSize,bestRecon,verts] = makePatchNum(num,graph1,startVert,K,Number_dipole,PhiTimeTemp)
ReconSigs=[];
vertnums=1;
verts=startVert;
vertsNew=verts;

for i=1:num
    ReconSig=K*sparse(verts,ones(length(verts),1),ones(length(verts),1),Number_dipole,1);
    ReconSigs=[ReconSigs ReconSig.*(ReconSig\PhiTimeTemp)];
    errs(i)=sqrt(sum((ReconSig.*(ReconSig\PhiTimeTemp)-PhiTimeTemp).^2));
    vertsNew=dialate(vertsNew,graph1,verts);
    verts=[verts vertsNew];
    vertnums=[vertnums length(verts)];
end

[bestErr,bestSize]=min(errs(6:end));
bestSize=bestSize+5;
bestRecon=ReconSigs(:,bestSize);
verts=verts(1:vertnums(bestSize));