% TEmporally Dependent Iterative Expansion
% Brandon Thio
% 1/9/2022

function RunTEDIEEphysSeizureIso(ind)
%% editable parameters
patients=144;
numSpikes=1;
samplingFreqs=256;
alphas=[0.1];
lambdas=[100];

%% Choose Sim
ct=0;
for p = 1:length(patients)
    for ns = 1:numSpikes(p)
        ct=ct+1;
        if ind==ct
            ID=patients(p);
            samplingFreq=samplingFreqs(p);
            whichSpike = ns;
        end
    end
end
rngseed=1;
ialpha=1;%ceil(ind/length(alphas));
ilambda=1;%mod(ind-1,length(lambdas))+1;
%% Tuneable Parameters
sigma=2;
range=-6:6;
alpha=alphas(ialpha);
lambda=lambdas(ilambda);
kernels=1/(2*sqrt(sigma*pi))*exp(-(range).^2/(2*sigma^2));% Gaussian
%% Parallelize
numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function
mkdir(pc_storage_dir);
pc = parcluster('local');
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead
tic
rng(rngseed);
cd(['Patient' num2str(ID)])

K=[];
load('LeadField/LeadField_Lower_ISO.mat')
K=LeadField;
normK=K./max(abs(K));
clear LeadField
load('work/OmitCorticalPoints2.mat')
if isfile('work/OmitNonCortex2.mat')
    load('work/OmitNonCortex2.mat')
    omitinds=[omitinds omits];
end
K(:,omitinds)=[];
K=K-mean(K);% fix average referencing after omitting channels
V=[];
load('Gradients2.mat')%V
V(:,omitinds)=[];
V(sum(V,2)~=0,:)=[];

Number_dipole = numel(K(1,:));
load('work/SmoothedBrainMesh2.mat')
TRI=scirunfield.face';
Vertice_Location=scirunfield.node;
elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
elemLoc(:,omitinds)=[];

disp('Loaded Data')
cd('..')
graph1=CreateElementGraph(V,elemLoc);
%% Loading The Noisy Scalp Potential and The True Underlying Source and
load(['Patient' num2str(ID) '/Ephys/SeizureTimeCourse_' num2str(whichSpike) '_1000.mat']);

noiseTimes=30*samplingFreq;
numTimes=length(filteredData(:,1))-noiseTimes;
Phi=filteredData';
PhiSig=Phi(:,(noiseTimes+1):(noiseTimes+numTimes));

scaleFactor=max(abs(PhiSig));
PhiSig=PhiSig./scaleFactor;
Number_sensor = size(K,1);
disp('Made Synthetic data')
%% Estimating Noise and Initializing
% Spike Time - half a second before and after spike peak
Noise_st   = 1;
Noise_only = Phi(:,Noise_st:noiseTimes);

% Noise Estimation
noise= sqrt(mean((Noise_only(:)).^2))*2;

%% Initializing the Solution + optimize per time
J_sol    = cell(numTimes,1);
Phis = cell(numTimes,1);
PhiSig=parallel.pool.Constant(PhiSig);
noise=parallel.pool.Constant(noise);
scaleFactor=parallel.pool.Constant(scaleFactor);
K=parallel.pool.Constant(K);
Number_dipole=parallel.pool.Constant(Number_dipole);
graph1=parallel.pool.Constant(graph1);
elemLoc=parallel.pool.Constant(elemLoc);
range=parallel.pool.Constant(range);
kernels=parallel.pool.Constant(kernels);
alpha=parallel.pool.Constant(alpha);
lambda=parallel.pool.Constant(lambda);
parfor i=1:numTimes
    PhiTime=PhiSig.Value(:,i);
    noiseFloor=noise.Value./scaleFactor.Value(i);
    ReconSig=zeros(length(PhiTime),1);
    corrs=0;
    PhiTemp=zeros(length(PhiTime),1);
    J_solTemp=[];
    % Pick dipoles that account for all the appreciable signals
    % at least one source
    [~,ind]=max(abs(corr(K.Value,PhiTime-ReconSig)));
    PhiTimeTemp=PhiTime-sum(PhiTemp,2);
    [Err,best,reconSig,~]=makePatchNum(100,graph1.Value,ind,K.Value,Number_dipole.Value,PhiTimeTemp);
    J_solTemp=[J_solTemp;[ind,best,1,Err]];% dipole loc,radius, scale,error
    PhiTemp=[PhiTemp reconSig];
    ReconSig=sum(PhiTemp,2);
    corrs=corr(PhiTime,ReconSig);
    while sqrt(mean((PhiTime-ReconSig).^2))>noiseFloor & corrs<0.9 & length(J_solTemp)<5
        [~,ind]=max(abs(corr(K.Value,PhiTime-ReconSig)));
        PhiTimeTemp=PhiTime-sum(PhiTemp,2);
        [Err,best,reconSig,~]=makePatchNum(100,graph1.Value,ind,K.Value,Number_dipole.Value,PhiTimeTemp);
        J_solTemp=[J_solTemp;[ind,best,1,Err]];% dipole loc,radius, scale,error
        PhiTemp=[PhiTemp reconSig];
        ReconSig=sum(PhiTemp,2);
        corrs=corr(PhiTime,ReconSig);
    end
    PhiTemp(:,1)=[];
    Phis{i}=PhiTemp;
    J_sol{i}=J_solTemp;
end

%% Figure out how many sources there are in the brain
clusters=[];
labels=cell(numTimes,1);
clusterct=0;
for i=1:numTimes
    if ~isempty(J_sol{i})
        omit=[];
        labels{i}=zeros(1,length(J_sol{i}(:,1)));
        for j=1:length(J_sol{i}(:,1))
            loc =elemLoc.Value(:,J_sol{i}(j,1));
            if isempty(clusters)
                clusters=loc;
                labels{i}(j)=1;
                clusterct=clusterct+1;
            else
                dists=sqrt(sum((clusters-loc).^2));
                
                for k = 1:length(dists)
                    if dists(k)<40
                        if ~any(labels{i}==k)
                            labels{i}(j)=k;
                        else
                            omit=[omit j];
                        end
                        
                    end
                end
                
                if all(dists>40)
                    clusters=[clusters loc];
                    clusterct = [clusterct 1];
                    labels{i}(j)=length(dists)+1;
                end
            end
        end
        J_sol{i}(omit,:)=[];
        Phis{i}(:,omit)=[];
        labels{i}(omit)=[];
        J_sol{i}(:,4)=inf;
    end
    
end
toc

%% Iteratively optimize across time and space
itererr=zeros(1,20);
J_solNew=J_sol;
PhisNew=Phis;
labelsNew=labels;
repeat=1;
prevErr=1e15;
currErr=0;
while ((prevErr-currErr)/prevErr)>0.01% | repeat<10
    if repeat>1
        prevErr=itererr(repeat-1);
    end
    % for repeat=1:20
    disp(['Iteration ' num2str(repeat)])
    %% Optimize the sources locations within and across time
    J_sol=parallel.pool.Constant(J_sol);
    Phis=parallel.pool.Constant(Phis);
    labels=parallel.pool.Constant(labels);
    parfor i=1:numTimes
        J_solNewTemp=J_sol.Value{i};
        PhisNewTemp=Phis.Value{i};
        labelsNewTemp=labels.Value{i};
        if ~isempty(J_sol.Value{i})
            if repeat == 1
                J_solNewTemp(:,4)=inf;
            end
            PhiTime=PhiSig.Value(:,i);
            labelTypes = unique(labels.Value{i});
            labelTypes(labelTypes==0)=[];
            for t=labelTypes
                if ~isempty(t)
                    rangeTemp=range.Value;
                    kernelsTemp=kernels.Value;
                    kernelsTemp((rangeTemp+i)<1)=[];
                    rangeTemp((rangeTemp+i)<1)=[];
                    kernelsTemp((rangeTemp+i)>numTimes)=[];
                    rangeTemp((rangeTemp+i)>numTimes)=[];
                    newLocs=[];
                    newSizes=[];
                    omit=[];
                    for k = 1:length(rangeTemp)
                        if any(labels.Value{i+rangeTemp(k)}==t)
                            which=find(labels.Value{i+rangeTemp(k)}==t);
                            newLocs=[newLocs kernelsTemp(k).*elemLoc.Value(:,J_sol.Value{i+rangeTemp(k)}(which,1))];
                            newSizes=[newSizes kernelsTemp(k).*J_sol.Value{i+rangeTemp(k)}(which,2)];
                        else
                            omit=[omit k];
                        end
                    end
                    kernelsTemp(omit)=[];
                    kernelsSum=sum(kernelsTemp);
                    distsTemp=sqrt(sum((elemLoc.Value-sum(newLocs,2)./kernelsSum).^2));
                    [~,ind]=min(distsTemp);
                    windowLoc=ind;
                    windowSize=sum(newSizes)./kernelsSum;
                    
                    % Gradient descent - euclidian space + size
                    prev = inf;
                    ct=1;
                    bestPhi=[];
                    while J_solNewTemp(1,4)<prev | ct<10
                        ct=ct+1;
                        prev=J_solNewTemp(1,4);
                        % Iteratively move source until it is converged
                        shiftamt=exp(-ct/2.5)*20;
                        for j=1:length(J_solNewTemp(:,1))
                            which=1:length(J_solNewTemp(:,1));
                            which(j)=[];
                            PhiTimeTemp=PhiTime-sum(Phis.Value{i}(:,which),2);
                            for k = 1:3 % search in XYZ
                                randval=rand();
                                shift=zeros(3,1);
                                shift(k)=shiftamt+2*(randval-0.5);%add stochasticity
                                dists=sqrt(sum(((elemLoc.Value(:,round(J_solNewTemp(j,1)))+shift)-elemLoc.Value).^2));
                                [~,ind]=min(dists);
                                distErr=sqrt(sum((elemLoc.Value(:,ind)-elemLoc.Value(:,windowLoc)).^2));
                                sizeErr=abs(windowSize-J_solNewTemp(j,2));
                                [BestSize,Err,ReconSig]=CalcCost3(max(30,J_solNewTemp(j,2)+5),graph1.Value,ind,K.Value,Number_dipole.Value,PhiTimeTemp,alpha.Value,lambda.Value,distErr,windowSize);
                                if Err>J_solNewTemp(j,4)
                                    shift(k)=-shift(k);
                                    dists=sqrt(sum(((elemLoc.Value(:,round(J_solNewTemp(j,1)))+shift)-elemLoc.Value).^2));
                                    [~,ind]=min(dists);
                                    distErr=sqrt(sum((elemLoc.Value(:,ind)-elemLoc.Value(:,windowLoc)).^2));
                                    [BestSize,Err,ReconSig]=CalcCost3(max(30,J_solNewTemp(j,2)+5),graph1.Value,ind,K.Value,Number_dipole.Value,PhiTimeTemp,alpha.Value,lambda.Value,distErr,windowSize);
                                end
                                if Err<J_solNewTemp(j,4)
                                    J_solNewTemp(j,1)=ind;
                                    J_solNewTemp(j,2)=BestSize;
                                    PhisNewTemp(:,j)=ReconSig;
                                    J_solNewTemp(:,4)=Err;
                                end
                            end
                        end
                    end
                end
            end
            % Pruning of intersecting patches of cortex where the lower
            % labled patch is kept
            prune=[];
            if length(J_solNewTemp(:,1))>1
                for ii = 1:(length(J_solNewTemp(:,1))-1)
                    verts1=makePatchOnly(J_solNewTemp(ii,2),graph1.Value,J_solNewTemp(ii,1));
                    for jj = (ii+1):length(J_solNewTemp(:,1))
                        verts2=makePatchOnly(J_solNewTemp(jj,2),graph1.Value,J_solNewTemp(jj,1));
                        if length(intersect(verts1,verts2))>0
                            if labelsNewTemp(ii)>labelsNewTemp(jj)
                                prune=[prune ii];
                            else
                                prune=[prune jj];
                            end
                        end
                    end
                end
            end
            J_solNewTemp(prune,:)=[];
            PhisNewTemp(:,prune)=[];
            labelsNewTemp(prune)=[];
        end
        J_solNew{i}=J_solNewTemp;
        PhisNew{i}=PhisNewTemp;
        labelsNew{i}=labelsNewTemp;
    end
    
    for i=1:numTimes
        if ~isempty(J_solNew{i})
            itererr(repeat)=itererr(repeat)+J_solNew{i}(end,4);
        end
    end
    J_sol=J_solNew;
    Phis=PhisNew;
    labels=labelsNew;
    currErr=itererr(repeat);
    disp(currErr)
    repeat=repeat+1;
end
save(['Patient' num2str(ID) '/Recons/J_sol_Ephys_Seizure_' num2str(whichSpike) '_1000_ISO.mat'],'J_sol','-v7.3');
toc






