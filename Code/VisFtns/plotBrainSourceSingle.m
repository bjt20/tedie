function plotBrainSourceSingle(data,TRI,Vertice_Location,viewvals)
% figure
scrnsz = get(0,'ScreenSize');
set(gcf,'outerposition',scrnsz)
clf
hold on
% data(data==0)=nan;

h1 = trisurf(TRI,Vertice_Location(1,:),Vertice_Location(2,:),Vertice_Location(3,:),data); colorbar
set(h1,'EdgeColor','None', 'FaceAlpha',1,'FaceLighting','phong');
light_position = [3 3 1];
light('Position',light_position);
light_position = [-3 -3 -1];
light('Position',light_position);
% colorbar;
axis off
% title(['Time ' num2str(times) ' Iteration ' num2str(weight_it)])
x_max = max(abs(data))+1;
% if isnan(x_max)
%     x_max = 1;
% end
caxis([0 x_max]);
% caxis([0 1000])
% perspect = [-1 0.25 0.5];
cmap1 = BWR2(200);
% cmap1 = parula(100);
% part1 = cmap1(1:33,:);
% part2 = cmap1(34:end,:);
mid_tran = gray(64);
mid = mid_tran(56:57,:);
cmap = [mid;cmap1(101:end,:)];
% cmap = [mid;cmap1];
% view(perspect)
axis equal
% view(-78.8362,36.2767)
% view(119,-41)
view(viewvals(1),viewvals(2))
grid off
colormap(cmap)
colorbar off
