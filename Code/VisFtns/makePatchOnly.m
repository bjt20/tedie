function [verts] = makePatchOnly(num,graph1,startVert)
verts=startVert;
vertsNew=verts;

for i=1:num
    vertsNew=dialate(vertsNew,graph1,verts);
    verts=[verts vertsNew];
end