function vertsNew = dialate(vertsNew,graph1,verts)
dialates=[];
for i=1:length(vertsNew)
    dialates=[dialates find(graph1(vertsNew(i),:)>0)];
end
dialates = unique(dialates);
vertsNew = setdiff(dialates, vertsNew);
vertsNew = setdiff(vertsNew, verts);