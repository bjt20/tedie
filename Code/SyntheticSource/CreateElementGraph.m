function graph1=CreateElementGraph(V,locs)
p1=mod(find(V'>0)-1,size(V,2))+1;
p2=mod(find(V'<0)-1,size(V,2))+1;
NeighborDists=sqrt(sum((locs(:,p1)-locs(:,p2)).^2));


graph1=sparse([p1;p2],[p2;p1],[NeighborDists';NeighborDists']);