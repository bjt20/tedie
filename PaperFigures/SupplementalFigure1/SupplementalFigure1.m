%% Supplemental Figure 1
load Data\AnisovIsoData.mat
figure(1)
% subplot(1,2,2)
colors=parula(2);
data1=jaccards(isoLevels==1);
data2=jaccards(isoLevels==2);
boxplot([data1 abs(data1-data2) data2])
% violin([data1(:) data2(:)],'facecolor',colors)
xticks([1 2 3])
xticklabels({'Anisotropic','Difference','Isotropic'});
a = get(gca,'XTickLabel');  
set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
ylabel('Jaccard Index','FontSize',20)
axis([0 4 0 1])

set(gcf,'outerposition',[1 1 1880 1080])
fileName='Figures\JaccardViolinSyntheticSourcesISO.pdf';
% print(gcf,fileName,'-bestfit','-dpdf')


figure(2)
% subplot(1,2,2)
colors=parula(2);
data1=Maps(isoLevels==1);
data2=Maps(isoLevels==2);
% violin([data1(:) data2(:)].*100,'facecolor',colors)
boxplot([data1 abs(data1-data2) data2].*100)
xticks([1 2 3])
xticklabels({'Anisotropic','Difference','Isotropic'});
a = get(gca,'XTickLabel');  
set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
ylabel('% Mapped','FontSize',20)
axis([0 4 0 100])

set(gcf,'outerposition',[1 1 1880 1080])
fileName='Figures\MappedViolinSyntheticSourcesISO.pdf';
% print(gcf,fileName,'-bestfit','-dpdf')


figure(3)
% subplot(1,2,2)
colors=parula(2);
data1=Errs(isoLevels==1);
data2=Errs(isoLevels==2);
% violin([data1(:) data2(:)],'facecolor',colors)
boxplot([data1 abs(data1-data2) data2])
xticks([1 2 3])
xticklabels({'Anisotropic','Difference','Isotropic'});  
set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
ylabel('Localization Error (mm)','FontSize',20)
axis([0 4 0 18])

set(gcf,'outerposition',[1 1 1880 1080])
fileName='Figures\LocErrViolinSyntheticSourcesISO.pdf';
% print(gcf,fileName,'-bestfit','-dpdf')
