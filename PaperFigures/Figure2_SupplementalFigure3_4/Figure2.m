addpath ../../Code/VisFtns/
%% Bad Outcome
tic
numSpikes=[1];%[4 6 6 2 6 12 8 6 5 5 3 6];
samplingFreqs=[256];
ct1=30;
for patientNum=1
    data3=[];
    patients=171;
    views=[1 0 1 1 1 0 1 1 1 1 0 0];% 1 is Left 0 is right
    patient=['Patient' num2str(patients(patientNum))];
    load(['Data/' patient '/Gradients2.mat'])%V
    
    load(['Data/' patient '/SmoothedBrainMesh2.mat'])
    TRI=scirunfield.face';
    Vertice_Location=scirunfield.node;
    elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
    
    load(['Data/' patient '/OmitCorticalPoints2.mat'])
    if isfile(['Data/' patient '/OmitNonCortex2.mat'])
        load(['Data/' patient '/OmitNonCortex2.mat'])
        omitinds=[omitinds omits];
    end
    elemLoc(:,omitinds)=[];
    V(:,omitinds)=[];
    V(sum(V,2)~=0,:)=[];
    graph1=CreateElementGraph(V,elemLoc);
    TRI(omitinds,:)=[];
    VertElemMap=sparse([1:length(TRI) 1:length(TRI) 1:length(TRI)],[TRI(:,1)' TRI(:,2)' TRI(:,3)'],1,length(TRI),length(Vertice_Location));
    for spikenum =4
        name = ['Data\Patient' num2str(patients(patientNum)) '\Seizure_' num2str((spikenum)) '.mat'];
        if isfile(name)
            load(name)
            name=['Recons\Patient' num2str(patients(patientNum)) '\J_sol_Ephys_Seizure_' num2str((spikenum)) '_1000_ISO.mat'];
            if isfile(name)
                load(name)
                %% Optimize over time
                % Visualize
                animation_frames={};
                allVerts=cell(samplingFreqs(patientNum),1);
                ct=0;
                startTimes= 36352;
                times=startTimes;
                which=find(max(abs(filteredData(startTimes:end,:)))>10000);
                filteredData(:,which)=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                thresh=min((std(filteredData(:))*3),300);
                while ct<100 
                    verts=[];
                    if ~isempty(J_sol{times}) & max(abs(filteredData(times+256*30,:)))>thresh
                        ct=ct+1;
                        for i=1:length(J_sol{times}(:,1))
                            verts=[verts makePatchOnly(J_sol{times}(i,2),graph1,J_sol{times}(i,1))];
                        end
                    end
                    allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc(1,:)));
                    times=times+1;
                end
                endTimes=times-1;
                %%
                for times=round((endTimes+startTimes)/2)
                    data=sum(vertcat(allVerts{(times-floor((endTimes-startTimes)/2)):(times+floor((endTimes-startTimes)/2))}));
%                     plotBrainSource3(full(data),TRI,Vertice_Location,0,filteredData,(times)/256+30,(endTimes-startTimes)/256,30+length(J_sol)/256,30,VertElemMap,mean(elemLoc(1,:))+2.5)
                    plotBrainSourceSingle(full(data),TRI,Vertice_Location,[-120 21])
                    hold on
                    resection=[0.2381  -26.0937   11.3539
                        -4.7544  -25.8324   11.4377
                        -9.7468  -25.5711   11.5215];
                    scatter3(resection(:,1),resection(:,2),resection(:,3),500,'yellow','filled')
                end
            end
        end
        ct1=ct1+1;
    end
end
% print('Figures/BadOutcomeWithResectedContacts.png','-dpng')
%% Good Outcome
for patientNum=1
    data3=[];
    patients=144;
    views=[1 0 1 1 1 0 1 1 1 1 0 0];% 1 is Left 0 is right
    patient=['Patient' num2str(patients(patientNum))];
    load(['Data/' patient '/Gradients2.mat'])%V
    
    load(['Data/' patient '/SmoothedBrainMesh2.mat'])
    TRI=scirunfield.face';
    Vertice_Location=scirunfield.node;
    elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
    
    load(['Data/' patient '/OmitCorticalPoints2.mat'])
    if isfile(['Data/' patient '/OmitNonCortex2.mat'])
        load(['Data/' patient '/OmitNonCortex2.mat'])
        omitinds=[omitinds omits];
    end
    elemLoc(:,omitinds)=[];
    V(:,omitinds)=[];
    V(sum(V,2)~=0,:)=[];
    graph1=CreateElementGraph(V,elemLoc);
    TRI(omitinds,:)=[];
    VertElemMap=sparse([1:length(TRI) 1:length(TRI) 1:length(TRI)],[TRI(:,1)' TRI(:,2)' TRI(:,3)'],1,length(TRI),length(Vertice_Location));
    for spikenum =1
        name = ['Data\Patient' num2str(patients(patientNum)) '\Seizure_' num2str((spikenum)) '.mat'];
        
        if isfile(name)
            load(name)
            name=['Recons\Patient' num2str(patients(patientNum)) '\J_sol_Ephys_Seizure_' num2str((spikenum)) '_1000_ISO.mat'];
            if isfile(name)
                load(name)
                %% Optimize over time
                % Visualize
                animation_frames={};
                allVerts=cell(samplingFreqs(patientNum),1);
                ct=0;
                startTimes= 46848;
                times=startTimes;
                which=find(max(abs(filteredData(startTimes:end,:)))>10000);
                filteredData(:,which)=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                thresh=min((std(filteredData(:))*3),300);
                while ct<100 
                    verts=[];
                    if ~isempty(J_sol{times}) & max(abs(filteredData(times+256*30,:)))>thresh
                        ct=ct+1;
                        for i=1:length(J_sol{times}(:,1))
                            verts=[verts makePatchOnly(J_sol{times}(i,2),graph1,J_sol{times}(i,1))];
                        end
                    end
                    allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc(1,:)));
                    times=times+1;
                end
                endTimes=times-1;
                %%
                for times=round((endTimes+startTimes)/2)
                    data=sum(vertcat(allVerts{(times-floor((endTimes-startTimes)/2)):(times+floor((endTimes-startTimes)/2))}));
%                     plotBrainSource3(full(data),TRI,Vertice_Location,0,filteredData,(times)/256+30,(endTimes-startTimes)/256,30+length(J_sol)/256,30,VertElemMap,mean(elemLoc(1,:))+2.5)
                    plotBrainSourceSingle(full(data),TRI,Vertice_Location,[-262 15])
                    hold on
                    resection=[22.5518    2.4090   -1.5760
                        27.3404    1.3628   -0.5886
                        32.1290    0.3165    0.3988
                        36.9175   -0.7297    1.3862
                        41.7061   -1.7759    2.3736
                        46.4947   -2.8222    3.3610
                        51.2832   -3.8684    4.3484
                        56.0718   -4.9146    5.3358
                        60.8604   -5.9609    6.3232
                        25.6632   -8.4774    3.5350
                        29.9097  -10.7855    4.8157
                        34.1562  -13.0936    6.0963
                        38.4027  -15.4017    7.3769
                        30.5836  -14.9459   10.2676
                        34.5262  -17.3470   12.1887];
                    scatter3(resection(:,1),resection(:,2),resection(:,3),500,'yellow','filled')
                end
            end
        end
        ct1=ct1+1;
    end
end
% print('Figures/GoodOutcomeWithResectedContacts.png','-dpng')
toc
%% Visualize SO calc
name = ['Data\Patient' num2str(144) '\Seizure_' num2str(2) '.mat'];
load(name)
figure
plot(linspace(0,length(filteredData)/512,length(filteredData)),filteredData,'k-')
hold on
plot([43000 43000]./512,[-4000 3000],'r-','LineWidth',2)
sigma=std(filteredData(:));
plot([0 200],[sigma.*3 sigma.*3],'b-','LineWidth',2)
plot([0 200],[sigma.*-3 sigma.*-3],'b-','LineWidth',2)
axis([80 160 -inf inf])
% print -dpng Figures/SOVis.png
%% other parts are in PlotConsistencySOZ.m