%% Code to make the boxplots and ROC for Figure 2. Also is used to make supplemental Figure 3
% Depending on the panel of figure 2 you want to make you will need to
% comment out different things.
% panel D requires temp=mindistsBest(i,:); to be uncommented.
% panel E requires temp=maxmindists(i,:); to be uncommented.
% The temporal lobe and extratemporal lobe ROCs are made by uncommenting
% tot(TL==1)=[]; and engel(TL==1)=[];
% You also need to set TL==1 for extratemporal lobe and TL==0 for temporal
% lobe isolation.
patients=[3 7 19 20 24 27 30 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
numSpikes=[4 4 2 3 5 5 5 3 5 3 3 5 5 1 2 5 3 3 5 5 3 5 5 5 5 4 3 5 3 0 2 4 5 5 3 2 5 5 5 5 5 3];
engel=[1 2 1 1 1 1 2 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3 ];
TL=[0 0 0 1 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];
% MTL=[0 0 0 1 0 1 1 0 0 1 1 0 1 0 1 1 0 1 1 0 0 1 0 0 1 0 0 1 1 1 0 0 0 0 0 0 0 1 1 1 0 0];
tots=[];
jitterVals=[];
cutoffVals=[];

cutoffThresh=[1 2 3 4 5 6 7 8 9 10 15 20 25 30 35 40 45 50];
jitters=[0 10 100 1000];
TL([11 30])=[];
% MTL([11 30])=[];
engel([11 30])=[];

aurocs=zeros(1,18);
accuracies=zeros(1,18);

for jitter=1% change for Supplemental Figure 4
    
    for cutoff=7%:18 % change for Supplemental Figure 4
    
    
        load(['Recons\SOZ_Resection_Cutoff_' num2str(cutoffThresh(cutoff)) '_Jitter_' num2str(jitters(jitter)) '_New.mat'])
        tot=[];
        engel2=[];
        TL2=[];
        for i=[1:10 12:29 31:length(patients)]
%             temp=mindistsBest(i,:); % Highest amplitude point -
%             Supplemental Figure 3
            temp=maxmindists(i,:); % Spatial extents - best for cutoff 7 - Figure 2
            temp(temp==-1)=[];
            tot=[tot median(temp)];
            jitterVals=[jitterVals jitter];
            cutoffVals=[cutoffVals cutoff];
        end
        tots=[tots tot'];
%         tot(TL==1)=[]; % if you want to subdivide by temporal lobe or not
%         engel(TL==1)=[];
        threshes=sort(tot);
        sensitivity=[];
        specificity=[];
        for i=1:length(threshes)
            sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
                (length(tot(engel==1)))];
            specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
        end
        accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
        aurocs(jitter,cutoff)=trapz(1-specificity,sensitivity);
        accuracies(jitter,cutoff)=max(accuracy);
    end
end
patients([11 30])=[];
figure
plot(cutoffThresh,aurocs)
% imagesc(aurocs)
figure
plot(cutoffThresh,accuracies)
% imagesc(accuracies)
[p,t,stats]=anovan(tot,{engel>1,TL},'model','interaction','varnames',{'Engel','Temporal'})
[h,p,ci,stats]=ttest2(tot(TL==1 & engel==1),tot(TL==1 & engel>1))
[h,p,ci,stats]=ttest2(tot(TL==0 & engel==1),tot(TL==0 & engel>1))

%%
dataBoxplot=NaN(length(patients),4);
dataBoxplot(1:length(tot(TL==1 & engel==1)),1)=tot(TL==1 & engel==1);
dataBoxplot(1:length(tot(TL==1 & engel>1)),2)=tot(TL==1 & engel>1);
dataBoxplot(1:length(tot(TL==0 & engel==1)),3)=tot(TL==0 & engel==1);
dataBoxplot(1:length(tot(TL==0 & engel>1)),4)=tot(TL==0 & engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0;0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1 & engel==1)))+rand(1,length(tot(TL==1 & engel==1))).*0.2-0.1,(tot(TL==1 & engel==1)),'ko')
plot(ones(1,length(tot(TL==1 & engel>1))).*2+rand(1,length(tot(TL==1 & engel>1))).*0.2-0.1,(tot(TL==1 & engel>1)),'ko')
plot(ones(1,length(tot(TL==0 & engel==1))).*3+rand(1,length(tot(TL==0 & engel==1))).*0.2-0.1,(tot(TL==0 & engel==1)),'ko')
plot(ones(1,length(tot(TL==0 & engel>1))).*4+rand(1,length(tot(TL==0 & engel>1))).*0.2-0.1,(tot(TL==0 & engel>1)),'ko')
axis([0 5 0 90])
xticks([1.5 3.5])
xticklabels({'Temporal Lobe','Extra Temporal'})
ylabel('Average Localization Error (mm)')
legend('Engel 1','Engel 2-4')
% print('Figures/LocalizationErrors_Engel_TL_Best.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(engel==1)),1)=tot(engel==1);
dataBoxplot(1:length(tot(engel>1)),2)=tot(engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(engel==1)))+rand(1,length(tot(engel==1))).*0.2-0.1,(tot(engel==1)),'ko')
plot(ones(1,length(tot(engel>1))).*2+rand(1,length(tot(engel>1))).*0.2-0.1,(tot(engel>1)),'ko')
axis([0.5 2.5 0 90])
xticks([1 2])
xticklabels({'Engel 1','Engel 2-4'})
ylabel('Min-Resected Contact Localization Error (mm)')
% print('Figures/LocalizationErrors_Engel_Best.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(TL==1)),1)=tot(TL==1);
dataBoxplot(1:length(tot(TL==0)),2)=tot(TL==0);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1)))+rand(1,length(tot(TL==1))).*0.2-0.1,(tot(TL==1)),'ko')
plot(ones(1,length(tot(TL==0))).*2+rand(1,length(tot(TL==0))).*0.2-0.1,(tot(TL==0)),'ko')
axis([0.5 2.5 0 90])
xticks([1 2])
xticklabels({'Temporal Lobe','Extratemporal Lobe'})
ylabel('Average Localization Error (mm)')
% print('Figures/LocalizationErrors_TL_Best.pdf','-dpdf','-painters')

%%
figure
plot(1-specificity,sensitivity)
axis square
% print('Figures/ROC_All_ETL.pdf','-dpdf','-painters')