
patients=[2 3 7 19 20 24 25 26 27 30 31 33];
jaccardTot=zeros(7,7);
MappedTot=zeros(7,7);
LocErrTot=zeros(7,7);
jaccards=cell(7,1);
Maps=cell(7,1);
Errs=cell(7,1);
for p = 1:9% Test on 10-12
    load(['Recons/Patient' num2str(patients(p)) '/Metrics.mat']);
    jaccardTot=jaccardTot+jaccard;
    MappedTot=MappedTot+mapped;
    LocErrTot=LocErrTot+dist;
    jaccards{p}=mean(jaccard,3);
    Maps{p}=mean(mapped,3);
    Errs{p}=mean(dist,3);
end
omit=find(sum(sum(jaccard))==0);
jaccardTot(:,:,omit)=[];
MappedTot(:,:,omit)=[];
LocErrTot(:,:,omit)=[];
figure
imagesc(mean(jaccardTot,3)./9)
alphas={5,10,20,50,100,200,500};
lambdas={0.01,0.02,0.05,0.1,0.2,0.5,1};
xticklabels(alphas)
xlabel('\lambda','FontSize',16)
yticklabels(lambdas)
ylabel('\alpha','FontSize',16)
colorbar
% print -dpng Figures\CrossValidation_JaccardIndex.png

figure
imagesc(mean(MappedTot,3)./9)
alphas={5,10,20,50,100,200,500};
lambdas={0.01,0.02,0.05,0.1,0.2,0.5,1};
xticklabels(alphas)
xlabel('\lambda','FontSize',16)
yticklabels(lambdas)
ylabel('\alpha','FontSize',16)
colorbar
% print -dpng Figures\CrossValidation_Mapped.png

figure
imagesc(mean(LocErrTot,3)./9)
alphas={5,10,20,50,100,200,500};
lambdas={0.01,0.02,0.05,0.1,0.2,0.5,1};
xticklabels(alphas)
xlabel('\lambda','FontSize',16)
yticklabels(lambdas)
ylabel('\alpha','FontSize',16)
colorbar
% print -dpng Figures\CrossValidation_LocErr.png
