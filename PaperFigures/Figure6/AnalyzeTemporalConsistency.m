%% Panel F
load('Recons/TemporalConsistency.mat')
figure
D1=[];
D2=[];
D3=[];
for i=1:46
    temp1=[];
    temp2=[];
    temp3=[];
    for j=1:5
        if jaccardTEDIE(i,j)>0 | jaccardsLORETA(i,j)>0 | jaccardIRES(i,j)>0
            temp1=[temp1 jaccardTEDIE(i,j)];
            temp2=[temp2 jaccardsLORETA(i,j)];
            temp3=[temp3 jaccardIRES(i,j)];
        end
    end
    D1=[D1 median(temp1)];
    D2=[D2 median(temp2)];
    D3=[D3 median(temp3)];
end
boxplot([D1' D2' D3'])
axis([0.5 3.5 0 1])
hold on
plot(ones(1,length(D1))+rand(1,length(D1)).*0.4-0.2,D1,'ko')
plot(ones(1,length(D2)).*2+rand(1,length(D1)).*0.4-0.2,D2,'ko')
plot(ones(1,length(D3)).*3+rand(1,length(D1)).*0.4-0.2,D3,'ko')
ylabel('Jaccard Index')
xticks([1 2 3])
xticklabels({'TEDIE','sLORETA','IRES'})
% print('Figures/TemporalConsistency.pdf','-dpdf','-painters')
kruskalwallis([D1' D2' D3'])