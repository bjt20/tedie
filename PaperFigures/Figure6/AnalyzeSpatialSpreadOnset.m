%% Panel E
load Recons\AnalyzedDiffusivity.mat
AT=[];
AS=[];
AI=[];
for i=1:42
    temp1=[];
    temp2=[];
    temp3=[];
    for j=1:5
        if accountedTEDIE(i,j)>-1
            temp1=[temp1 accountedTEDIE(i,j)];
            temp2=[temp2 accountedsLORETA(i,j)];
            temp3=[temp3 accountedIRES(i,j)];
        end
    end
    AT=[AT median(temp1)];
    AS=[AS median(temp2)];
    AI=[AI median(temp3)];
end
figure
boxplot([AT' AS' AI'])
hold on
plot(ones(1,length(AT))+rand(1,length(AT)).*0.4-0.2,AT,'ko')
plot(ones(1,length(AT)).*2+rand(1,length(AT)).*0.4-0.2,AS,'ko')
plot(ones(1,length(AT)).*3+rand(1,length(AT)).*0.4-0.2,AI,'ko')
axis([0.5 3.5 0 90])
% print(['Figures/SpatialSpreadSeizureOnsetAllAlgs50.pdf'],'-dpdf','-painters')
kruskalwallis([AT' AS' AI'])
[h,p]=signrank(AT,AS)
[h,p]=signrank(AT,AI)
anova1([AT' AS' AI'])
[h,p] = ttest(AT,AS)
[h,p] = ttest(AT,AI)