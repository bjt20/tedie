addpath('../../Code/VisFtns/')
%% Panel A
numSpikes=[1];%[4 6 6 2 6 12 8 6 5 5 3 6];
ct1=30;
for patientNum=1
    data3=[];
    patients=150;
    views=[1 0 1 1 1 0 1 1 1 1 0 0];% 1 is Left 0 is right
    patient=['Patient' num2str(patients(patientNum))];
    load(['Data/' patient '/Gradients2.mat'])%V
    
    load(['Data/' patient '/SmoothedBrainMesh2.mat'])
    TRI=scirunfield.face';
    Vertice_Location=scirunfield.node;
    elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
    
    load(['Data/' patient '/OmitCorticalPoints2.mat'])
    if isfile(['Data/' patient '/OmitNonCortex2.mat'])
        load(['Data/' patient '/OmitNonCortex2.mat'])
        omitinds=[omitinds omits];
    end
    elemLoc(:,omitinds)=[];
    V(:,omitinds)=[];
    V(sum(V,2)~=0,:)=[];
    graph1=CreateElementGraph(V,elemLoc);
    TRI(omitinds,:)=[];
    VertElemMap=sparse([1:length(TRI) 1:length(TRI) 1:length(TRI)],[TRI(:,1)' TRI(:,2)' TRI(:,3)'],1,length(TRI),length(Vertice_Location));
    for spikenum =1
        name = ['Data\Patient' num2str(patients(patientNum)) '\Seizure_' num2str((spikenum)) '.mat'];
        if isfile(name)
            load(name)
            name=['Recons\Patient' num2str(patients(patientNum)) '\J_sol_Ephys_Seizure_' num2str((spikenum)) '_1000_ISO.mat'];
            if isfile(name)
                load(name)
                %% Optimize over time
                % Visualize
                animation_frames={};
                allVerts=cell(samplingFreqs(patientNum),1);
                ct=0;
                startTimes= 31488;
                times=startTimes;
                which=find(max(abs(filteredData(startTimes:end,:)))>10000);
                filteredData(:,which)=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                thresh=min((std(filteredData(:))*3),300);
                while ct<100 
                    verts=[];
                    if ~isempty(J_sol{times}) & max(abs(filteredData(times+256*30,:)))>thresh
                        ct=ct+1;
                        for i=1:length(J_sol{times}(:,1))
                            verts=[verts makePatchOnly(J_sol{times}(i,2),graph1,J_sol{times}(i,1))];
                        end
                    end
                    allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc(1,:)));
                    times=times+1;
                end
                endTimes=times-1;
                %%
                for times=round((endTimes+startTimes)/2)
                    data=sum(vertcat(allVerts{(times-floor((endTimes-startTimes)/2)):(times+floor((endTimes-startTimes)/2))}));
%                     plotBrainSource3(full(data),TRI,Vertice_Location,0,filteredData,(times)/256+30,(endTimes-startTimes)/256,30+length(J_sol)/256,30,VertElemMap,mean(elemLoc(1,:))+2.5)
                    plotBrainSourceSingle(full(data),TRI,Vertice_Location,[114 16])
                    hold on
                    scatter3(32.1424,2.9157,54.3808,500,'yellow','filled')
                end
            end
        end
        ct1=ct1+1;
    end
end
% print -dpng Figures/InconsistentIRES.png

%% TEDIE Portion of Panel D
patients=[3 7 19 20 24 27 30 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
numSpikes=[4 4 2 3 5 5 5 3 5 3 3 5 5 1 2 5 3 3 5 5 3 5 5 5 5 4 3 5 3 0 2 4 5 5 3 2 5 5 5 5 5 3];
engel=[1 2 1 1 1 1 2 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3 ];
TL=[0 0 0 1 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];
tots=[];

cutoffThresh=[1 2 3 4 5 6 7 8 9 10 15 20 25 30 35 40 45 50];
jitters=[0 10 100 1000];
TL([11 30])=[];
engel([11 30])=[];

jitter=1;%:4
cutoff=7;%:18%:18
load(['Recons\SOZ_Resection_Cutoff_' num2str(cutoffThresh(cutoff)) '_Jitter_' num2str(jitters(jitter)) '_New.mat'])
tot=[];
engel2=[];
TL2=[];
for i=[1:10 12:29 31:length(patients)]
    temp=mindistsBest(i,:);
    %             temp=mindists(i,:);
    temp(temp==-1)=[];
    tot=[tot median(temp)];
end
totTEDIE=tot;
threshes=sort(tot);
sensitivity=[];
specificity=[];
for i=1:length(threshes)
    sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
        (length(tot(engel==1)))];
    specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
end
accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
aurocs=trapz(1-specificity,sensitivity)
accuracies=max(accuracy);
patients([11 30])=[];
[p,t,stats]=anovan(tot,{engel>1,TL},'model','interaction','varnames',{'Engel','Temporal'});

dataBoxplot=NaN(length(patients),4);
dataBoxplot(1:length(tot(TL==1 & engel==1)),1)=tot(TL==1 & engel==1);
dataBoxplot(1:length(tot(TL==1 & engel>1)),2)=tot(TL==1 & engel>1);
dataBoxplot(1:length(tot(TL==0 & engel==1)),3)=tot(TL==0 & engel==1);
dataBoxplot(1:length(tot(TL==0 & engel>1)),4)=tot(TL==0 & engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0;0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1 & engel==1)))+rand(1,length(tot(TL==1 & engel==1))).*0.2-0.1,(tot(TL==1 & engel==1)),'ko')
plot(ones(1,length(tot(TL==1 & engel>1))).*2+rand(1,length(tot(TL==1 & engel>1))).*0.2-0.1,(tot(TL==1 & engel>1)),'ko')
plot(ones(1,length(tot(TL==0 & engel==1))).*3+rand(1,length(tot(TL==0 & engel==1))).*0.2-0.1,(tot(TL==0 & engel==1)),'ko')
plot(ones(1,length(tot(TL==0 & engel>1))).*4+rand(1,length(tot(TL==0 & engel>1))).*0.2-0.1,(tot(TL==0 & engel>1)),'ko')
axis([0 5 0 80])
xticks([1.5 3.5])
xticklabels({'Temporal Lobe','Extra Temporal'})
ylabel('Average Localization Error (mm)')
legend('Engel 1','Engel 2-4')
% print('Figures/LocalizationErrors_Engel_TL_max.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(engel==1)),1)=tot(engel==1);
dataBoxplot(1:length(tot(engel>1)),2)=tot(engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(engel==1)))+rand(1,length(tot(engel==1))).*0.2-0.1,(tot(engel==1)),'ko')
plot(ones(1,length(tot(engel>1))).*2+rand(1,length(tot(engel>1))).*0.2-0.1,(tot(engel>1)),'ko')
axis([0.5 2.5 0 80])
xticks([1 2])
xticklabels({'Engel 1','Engel 2-4'})
ylabel('Min-Resected Contact Localization Error (mm)')
% print('Figures/LocalizationErrors_Engel_max.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(TL==1)),1)=tot(TL==1);
dataBoxplot(1:length(tot(TL==0)),2)=tot(TL==0);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1)))+rand(1,length(tot(TL==1))).*0.2-0.1,(tot(TL==1)),'ko')
plot(ones(1,length(tot(TL==0))).*2+rand(1,length(tot(TL==0))).*0.2-0.1,(tot(TL==0)),'ko')
axis([0.5 2.5 0 80])
xticks([1 2])
xticklabels({'Temporal Lobe','Extratemporal Lobe'})
ylabel('Average Localization Error (mm)')
% print('Figures/LocalizationErrors_TL_max.pdf','-dpdf','-painters')


%% sLORETA portion of Panel D
toc
patients=[3 7 19 20 24 27 30 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
engel=[1 2 1 1 1 1 2 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3 ];
TL=[0 0 0 1 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];

TL([11 30])=[];
engel([11 30])=[];

load(['Recons/SOZ_Resection_sLORETA.mat'])
tot=[];
engel2=[];
TL2=[];
for i=[1:10 12:29 31:length(patients)]
%     temp=meanmindists(i,:)+meanmindists2(i,:);
    temp=mindistsBest(i,:);
    %             temp=mindists(i,:);
%     temp(temp==-2)=[];
    temp(temp==-1)=[];
    tot=[tot median(temp)];
end
totsLORETA=tot;
threshes=sort(tot);
sensitivity=[];
specificity=[];
for i=1:length(threshes)
    sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
        (length(tot(engel==1)))];
    specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
end
accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
aurocs=trapz(1-specificity,sensitivity)
accuracies=max(accuracy);
patients([11 30])=[];
[p,t,stats]=anovan(tot,{engel>1,TL},'model','interaction','varnames',{'Engel','Temporal'});

dataBoxplot=NaN(length(patients),4);
dataBoxplot(1:length(tot(TL==1 & engel==1)),1)=tot(TL==1 & engel==1);
dataBoxplot(1:length(tot(TL==1 & engel>1)),2)=tot(TL==1 & engel>1);
dataBoxplot(1:length(tot(TL==0 & engel==1)),3)=tot(TL==0 & engel==1);
dataBoxplot(1:length(tot(TL==0 & engel>1)),4)=tot(TL==0 & engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0;0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1 & engel==1)))+rand(1,length(tot(TL==1 & engel==1))).*0.2-0.1,(tot(TL==1 & engel==1)),'ko')
plot(ones(1,length(tot(TL==1 & engel>1))).*2+rand(1,length(tot(TL==1 & engel>1))).*0.2-0.1,(tot(TL==1 & engel>1)),'ko')
plot(ones(1,length(tot(TL==0 & engel==1))).*3+rand(1,length(tot(TL==0 & engel==1))).*0.2-0.1,(tot(TL==0 & engel==1)),'ko')
plot(ones(1,length(tot(TL==0 & engel>1))).*4+rand(1,length(tot(TL==0 & engel>1))).*0.2-0.1,(tot(TL==0 & engel>1)),'ko')
axis([0 5 0 80])
xticks([1.5 3.5])
xticklabels({'Temporal Lobe','Extra Temporal'})
ylabel('Average Localization Error (mm)')
legend('Engel 1','Engel 2-4')
% print('Figures/LocalizationErrors_Engel_TL_sLORETA_max.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(engel==1)),1)=tot(engel==1);
dataBoxplot(1:length(tot(engel>1)),2)=tot(engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(engel==1)))+rand(1,length(tot(engel==1))).*0.2-0.1,(tot(engel==1)),'ko')
plot(ones(1,length(tot(engel>1))).*2+rand(1,length(tot(engel>1))).*0.2-0.1,(tot(engel>1)),'ko')
axis([0.5 2.5 0 80])
xticks([1 2])
xticklabels({'Engel 1','Engel 2-4'})
ylabel('Min-Resected Contact Localization Error (mm)')
% print('Figures/LocalizationErrors_Engel_Min_sLORETA_max.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(TL==1)),1)=tot(TL==1);
dataBoxplot(1:length(tot(TL==0)),2)=tot(TL==0);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1)))+rand(1,length(tot(TL==1))).*0.2-0.1,(tot(TL==1)),'ko')
plot(ones(1,length(tot(TL==0))).*2+rand(1,length(tot(TL==0))).*0.2-0.1,(tot(TL==0)),'ko')
axis([0.5 2.5 0 80])
xticks([1 2])
xticklabels({'Temporal Lobe','Extratemporal Lobe'})
ylabel('Average Localization Error (mm)')
% print('Figures/LocalizationErrors_TL_sLORETA_max.pdf','-dpdf','-painters')
%% Panel B
ID=150;
load(['Data/Patient' num2str(ID) '/SmoothedBrainMesh2.mat'])
TRI=scirunfield.face';
Vertice_Location=scirunfield.node;
load(['Data/Patient' num2str(ID) '/OmitCorticalPoints2.mat'])
if isfile(['Data/Patient' num2str(ID) '/OmitNonCortex2.mat'])
    load(['Data/Patient' num2str(ID) '/OmitNonCortex2.mat'])
    omitinds=[omitinds omits];
end
TRI(omitinds,:)=[];
name = ['Recons/Patient' num2str(ID) '/J_sol_SLORETA_SeizureOnset1_Thresh50_ISOVec.mat'];
load(name)
data=LIM./max(LIM).*100;
data(data<50)=0;
figure
plotBrainSourceSingle(full(data),TRI,Vertice_Location,[114 16])
hold on
scatter3(32.1424,2.9157,54.3808,500,'yellow','filled')
% print -dpng Figures/InconsistentSLORETA.png


%% IRES portion of Panel D
patients=[3 7 19 20 24 27 30 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
engel=[1 2 1 1 1 1 2 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3 ];
TL=[0 0 0 1 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];

TL([11 30])=[];
engel([11 30])=[];

load(['Recons/SOZ_Resection_IRES.mat'])
tot=[];
engel2=[];
TL2=[];
for i=[1:10 12:29 31:length(patients)]
    temp=mindistsBest(i,:);
%     temp=maxmindists(i,:);
    %             temp=mindists(i,:);
    temp(temp==-1)=[];
    tot=[tot median(temp)];
end
totIRES=tot;
threshes=sort(tot);
sensitivity=[];
specificity=[];
for i=1:length(threshes)
    sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
        (length(tot(engel==1)))];
    specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
end
accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
aurocs=trapz(1-specificity,sensitivity)
accuracies=max(accuracy);
patients([11 30])=[];
[p,t,stats]=anovan(tot,{engel>1,TL},'model','interaction','varnames',{'Engel','Temporal'});

dataBoxplot=NaN(length(patients),4);
dataBoxplot(1:length(tot(TL==1 & engel==1)),1)=tot(TL==1 & engel==1);
dataBoxplot(1:length(tot(TL==1 & engel>1)),2)=tot(TL==1 & engel>1);
dataBoxplot(1:length(tot(TL==0 & engel==1)),3)=tot(TL==0 & engel==1);
dataBoxplot(1:length(tot(TL==0 & engel>1)),4)=tot(TL==0 & engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0;0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1 & engel==1)))+rand(1,length(tot(TL==1 & engel==1))).*0.2-0.1,(tot(TL==1 & engel==1)),'ko')
plot(ones(1,length(tot(TL==1 & engel>1))).*2+rand(1,length(tot(TL==1 & engel>1))).*0.2-0.1,(tot(TL==1 & engel>1)),'ko')
plot(ones(1,length(tot(TL==0 & engel==1))).*3+rand(1,length(tot(TL==0 & engel==1))).*0.2-0.1,(tot(TL==0 & engel==1)),'ko')
plot(ones(1,length(tot(TL==0 & engel>1))).*4+rand(1,length(tot(TL==0 & engel>1))).*0.2-0.1,(tot(TL==0 & engel>1)),'ko')
axis([0 5 0 80])
xticks([1.5 3.5])
xticklabels({'Temporal Lobe','Extra Temporal'})
ylabel('Average Localization Error (mm)')
legend('Engel 1','Engel 2-4')
% print('Figures/LocalizationErrors_Engel_TL_IRES_max.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(engel==1)),1)=tot(engel==1);
dataBoxplot(1:length(tot(engel>1)),2)=tot(engel>1);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(engel==1)))+rand(1,length(tot(engel==1))).*0.2-0.1,(tot(engel==1)),'ko')
plot(ones(1,length(tot(engel>1))).*2+rand(1,length(tot(engel>1))).*0.2-0.1,(tot(engel>1)),'ko')
axis([0.5 2.5 0 80])
xticks([1 2])
xticklabels({'Engel 1','Engel 2-4'})
ylabel('Min-Resected Contact Localization Error (mm)')
% print('Figures/LocalizationErrors_Engel_Min_IRES_max.pdf','-dpdf','-painters')

dataBoxplot=NaN(length(patients),2);
dataBoxplot(1:length(tot(TL==1)),1)=tot(TL==1);
dataBoxplot(1:length(tot(TL==0)),2)=tot(TL==0);
figure
boxplot(dataBoxplot,'Colors',[0 0 1;1 0 0])
hold on
plot(ones(1,length(tot(TL==1)))+rand(1,length(tot(TL==1))).*0.2-0.1,(tot(TL==1)),'ko')
plot(ones(1,length(tot(TL==0))).*2+rand(1,length(tot(TL==0))).*0.2-0.1,(tot(TL==0)),'ko')
axis([0.5 2.5 0 80])
xticks([1 2])
xticklabels({'Temporal Lobe','Extratemporal Lobe'})
ylabel('Average Localization Error (mm)')
% print('Figures/LocalizationErrors_TL_IRES_max.pdf','-dpdf','-painters')
%% Panel C
ID=150;
load(['Data/Patient' num2str(ID) '/SmoothedBrainMesh2.mat'])
TRI=scirunfield.face';
Vertice_Location=scirunfield.node;
load(['Data/Patient' num2str(ID) '/OmitCorticalPoints2.mat'])
if isfile(['Data/Patient' num2str(ID) '/OmitNonCortex2.mat'])
    load(['Data/Patient' num2str(ID) '/OmitNonCortex2.mat'])
    omitinds=[omitinds omits];
end
TRI(omitinds,:)=[];
name = ['Recons/Patient' num2str(ID) '/J_sol_IRES_SeizureOnset_1_1000_ISO_alpha_1.mat'];
load(name)
data=abs(J_sol)./max(abs(J_sol)).*100;
% data(data<90)=0;
figure
plotBrainSourceSingle(full(data),TRI,Vertice_Location,[114 16])
hold on
scatter3(32.1424,2.9157,54.3808,500,'yellow','filled')
% print -dpng Figures/InconsistentIRES.png

%%
t=[totTEDIE' totsLORETA' totIRES'];
engels=([engel' engel' engel']==1).*1;
algorithm=[ones(40,1) ones(40,1).*2 ones(40,1).*3];
patients=repmat((1:40)',1,3);
anovan(t(:),{algorithm(:),engels(:)},'varnames',{'Algorithm','Engel'})
kruskalwallis(t)
[h,p]=ttest(totTEDIE(engel==1)',totsLORETA(engel==1)')
[h,p]=ttest(totTEDIE(engel==1)',totIRES(engel==1)')
[h,p]=signrank(totTEDIE(engel==1)',totsLORETA(engel==1)')
[h,p]=signrank(totTEDIE(engel==1)',totIRES(engel==1)')
%% continued in AnalyzeSpatialSpreadOnset.m and AnalyzeTemporalConsistency.m