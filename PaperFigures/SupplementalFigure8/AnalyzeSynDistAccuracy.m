load data/CompiledOutputsAdditionalSimsSyn.mat
%% Supplemental Figure 8
jaccard = outputComp.Jaccard;
com = outputComp.COM;
mapped = outputComp.Mapped;
dist = outputComp.ElecDists;

jaccard(3,:,:,:,:)=[];
com(3,:,:,:,:)=[];
mapped(3,:,:,:,:)=[];
dist(3,:,:,:,:)=[];

colors = [1 0 0;0 1 0;0 0 1];
for i = 1
    for j = 1
        figure
        hold on
        for k = 1:3
            plot(mean(squeeze(dist(:,k,i,j,:)),2),mean(squeeze(jaccard(:,k,i,j,:)),2),'o','Color',colors(k,:))
        end
    end
end
axis([0 45 0 1])
xlabel('Source to Electrode Distance (mm)')
ylabel('Jaccard Index')

print -dpdf ../Figures/JaccardSynDistAcc.pdf

for i = 1
    for j = 1
        figure
        hold on
        for k = 1:3
            plot(mean(squeeze(dist(:,k,i,j,:)),2),mean(squeeze(mapped(:,k,i,j,:)),2).*100,'o','Color',colors(k,:))
        end
    end
end
axis([0 45 0 100])
xlabel('Source to Electrode Distance (mm)')
ylabel('% Mapped')
print -dpdf ../Figures/MappedSynDistAcc.pdf

for i = 1
    for j = 1
        figure
        hold on
        for k = 1:3
            plot(mean(squeeze(dist(:,k,i,j,:)),2),mean(squeeze(com(:,k,i,j,:)),2),'o','Color',colors(k,:))
        end
    end
end
axis([0 45 0 30])
legend('6 dB','10 dB','20 dB')
xlabel('Source to Electrode Distance (mm)')
ylabel('Center of Mass Error (mm)')
print -dpdf ../Figures/COMSynDistAcc.pdf
