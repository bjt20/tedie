addpath('../../Code/VisFtns')
%% Consistent Seizure Onsets - Panel A
tic
numSpikes=[1];
ct1=30;
for patientNum=1
    data3=[];
    patients=177;
    views=[1 0 1 1 1 0 1 1 1 1 0 0];% 1 is Left 0 is right
    patient=['Patient' num2str(patients(patientNum))];
    load(['Data/' patient '/Gradients2.mat'])%V
    
    load(['Data/' patient '/SmoothedBrainMesh2.mat'])
    TRI=scirunfield.face';
    Vertice_Location=scirunfield.node;
    elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
    
    load(['Data/' patient '/OmitCorticalPoints2.mat'])
    if isfile(['Data/' patient '/OmitNonCortex2.mat'])
        load(['Data/' patient '/OmitNonCortex2.mat'])
        omitinds=[omitinds omits];
    end
    elemLoc(:,omitinds)=[];
    V(:,omitinds)=[];
    V(sum(V,2)~=0,:)=[];
    graph1=CreateElementGraph(V,elemLoc);
    TRI(omitinds,:)=[];
    VertElemMap=sparse([1:length(TRI) 1:length(TRI) 1:length(TRI)],[TRI(:,1)' TRI(:,2)' TRI(:,3)'],1,length(TRI),length(Vertice_Location));
    for spikenum =1 % 3 % Change for Seizure 1 and 3
        if patients>100
            name = ['Data\Patient' num2str(patients(patientNum)) '\Seizure_' num2str((spikenum)) '.mat'];
        else
            name = ['Data\Patient' num2str(patients(patientNum)) '\Ephys\SeizureTimeCourse_' num2str((spikenum)) '_1000.mat'];
        end
        if isfile(name)
            load(name)
            name=['Recons\Patient' num2str(patients(patientNum)) '\J_sol_Ephys_Seizure_' num2str((spikenum)) '_1000_ISO.mat'];
            if isfile(name)
                load(name)
                %% Optimize over time
                % Visualize
                animation_frames={};
                allVerts=cell(1,1);
                ct=0;
                startTimes= 32768; %32768, 35072;% Change for Seizure 1 and 3
                times=startTimes;
                which=find(max(abs(filteredData(startTimes:end,:)))>10000);
                filteredData(:,which)=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                thresh=min((std(filteredData(:))*3),300);
                while ct<100 
                    verts=[];
                    if ~isempty(J_sol{times}) & max(abs(filteredData(times+256*30,:)))>thresh
                        ct=ct+1;
                        for i=1:length(J_sol{times}(:,1))
                            verts=[verts makePatchOnly(J_sol{times}(i,2),graph1,J_sol{times}(i,1))];
                        end
                    end
                    allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc(1,:)));
                    times=times+1;
                end
                endTimes=times-1;
                %%
                for times=round((endTimes+startTimes)/2)
                    data=sum(vertcat(allVerts{(times-floor((endTimes-startTimes)/2)):(times+floor((endTimes-startTimes)/2))}));
                    plotBrainSourceSingle(full(data),TRI,Vertice_Location,[-180 -90])
                    
                end
            end
        end
        ct1=ct1+1;
    end
end
% print('Figures/ConsistentSeizures2.png','-dpng')
%% Inconsistent Seizure Onsets - Panel B
tic
numSpikes=[1];
ct1=30;
for patientNum=1
    data3=[];
    patients=151;
    views=[1 0 1 1 1 0 1 1 1 1 0 0];% 1 is Left 0 is right
    patient=['Patient' num2str(patients(patientNum))];
    load(['Data/' patient '/Gradients2.mat'])%V
    
    load(['Data/' patient '/SmoothedBrainMesh2.mat'])
    TRI=scirunfield.face';
    Vertice_Location=scirunfield.node;
    elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
    
    load(['Data/' patient '/OmitCorticalPoints2.mat'])
    if isfile(['Data/' patient '/OmitNonCortex2.mat'])
        load(['Data/' patient '/OmitNonCortex2.mat'])
        omitinds=[omitinds omits];
    end
    elemLoc(:,omitinds)=[];
    V(:,omitinds)=[];
    V(sum(V,2)~=0,:)=[];
    graph1=CreateElementGraph(V,elemLoc);
    TRI(omitinds,:)=[];
    VertElemMap=sparse([1:length(TRI) 1:length(TRI) 1:length(TRI)],[TRI(:,1)' TRI(:,2)' TRI(:,3)'],1,length(TRI),length(Vertice_Location));
    for spikenum =4 % 5, 2, 4% Change for Seizure 5, 2, and 4
        if patients>100
            name = ['Data\Patient' num2str(patients(patientNum)) '\Seizure_' num2str((spikenum)) '.mat'];
        else
            name = ['Data\Patient' num2str(patients(patientNum)) '\Ephys\SeizureTimeCourse_' num2str((spikenum)) '_1000.mat'];
        end
        if isfile(name)
            load(name)
            name=['Recons\Patient' num2str(patients(patientNum)) '\J_sol_Ephys_Seizure_' num2str((spikenum)) '_1000_ISO.mat'];
            if isfile(name)
                load(name)
                %% Optimize over time
                % Visualize
                animation_frames={};
                allVerts=cell(1,1);
                ct=0;
                startTimes= 32512; %38912, 31642, 32512;% Change for Seizure 5, 2, and 4
                times=startTimes;
                which=find(max(abs(filteredData(startTimes:end,:)))>10000);
                filteredData(:,which)=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                filteredData(:,[find(sum(abs(filteredData))==max(sum(abs(filteredData))))])=[];
                thresh=min((std(filteredData(:))*3),300);
                while ct<100 
                    verts=[];
                    if ~isempty(J_sol{times}) & max(abs(filteredData(times+256*30,:)))>thresh
                        ct=ct+1;
                        for i=1:length(J_sol{times}(:,1))
                            verts=[verts makePatchOnly(J_sol{times}(i,2),graph1,J_sol{times}(i,1))];
                        end
                    end
                    allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc(1,:)));
                    times=times+1;
                end
                endTimes=times-1;
                %%
                for times=round((endTimes+startTimes)/2)
                    data=sum(vertcat(allVerts{(times-floor((endTimes-startTimes)/2)):(times+floor((endTimes-startTimes)/2))}));
%                     plotBrainSource3(full(data),TRI,Vertice_Location,0,filteredData,(times)/256+30,(endTimes-startTimes)/256,30+length(J_sol)/256,30,VertElemMap,mean(elemLoc(1,:))+2.5)
                    plotBrainSourceSingle(full(data),TRI,Vertice_Location,[-180 -90])
                    
                end
            end
        end
        ct1=ct1+1;
    end
end
%print('Figures/InonsistentSeizures4.png','-dpng')
%% Panels C-D
% Load Data
patients=[2 3 7 19 20 24 25 26 27 30 31 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
Engels=[4 1 2 1 1 1 4 4 1 2 3 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3];
TL=[1 0 0 0 1 1 0 0 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];
numSpikes=[4 4 4 2 3 12 8 6 5 5 3 3 5 3 3 5 5 1 2 5 3 3 5 5 3 5 5 5 5 4 3 5 3 0 2 4 5 5 3 2 5 5 5 5 5 3];
cutoffThresh=[1 2 3 4 5 6 7 8 9 10 15 20 25 30 35 40 45 50];
jitters=[0 1 2 5 10 20 50 100 200 500 1000];%[0 10 20 50 100 200 500 1000 2000];
for jitter=1%1:9
    for trials=11
        load(['Recons/SOZ_Loc_Cutoff_' num2str(cutoffThresh(trials)) '_Jitter_' num2str(jitters(jitter)) '.mat'])
        for patientNum=1:length(patients)
            avg=mean(horzcat(AvgLoc{patientNum,1:numSpikes(patientNum)}),2);
            dists=[];
            for spikenum =1:numSpikes(patientNum)
                if length(AvgLoc{patientNum,spikenum})==3
                    dists=[dists sqrt(sum((avg-AvgLoc{patientNum,spikenum}).^2))];
                end
            end
            avgdists(patientNum)=mean(dists);
        end
    end
end
patients(isnan(avgdists))=[];
Engels(isnan(avgdists))=[];
TL(isnan(avgdists))=[];
avgdists(isnan(avgdists))=[];
% Analyze all the data together
figure
data=NaN(43,2);
data(Engels==1,1)=avgdists(Engels==1)';
data(Engels>1,2)=avgdists(Engels>1)';
boxplot(data)
hold on
plot(ones(1,length(data(Engels==1)))+rand(1,length(data(Engels==1))).*0.2-0.1,(data(Engels==1)),'ko')
plot(ones(1,length(data(Engels>1,2))).*2+rand(1,length(data(Engels>1,2))).*0.2-0.1,(data(Engels>1,2)),'ko')

% print(['Figures/SOZConsistencyCDS.pdf'],'-dpdf','-painters')
% Generate ROC for all the data
sensitivities=[];
specificities=[];
for b=1:1000
    
    randvals=randi(length(avgdists),length(avgdists),1);
    threshes=avgdists(randvals);
    engel2=Engels(randvals);
    while length(threshes(engel2==1))==0 | length(threshes(engel2==1))==length(avgdists)
        randvals=randi(length(avgdists),length(avgdists),1);
        threshes=avgdists(randvals);
        engel2=Engels(randvals);
    end
    threshes2=sort(threshes);
    sensitivity=[];
    specificity=[];
    for i=1:length(threshes)
        sensitivity=[sensitivity length(threshes(engel2==1 & threshes<=threshes2(i)))./...
            (max(length(threshes(engel2==1)),0))];
        specificity=[specificity length(threshes(engel2>1&  threshes>threshes2(i)))./max(length(threshes(engel2>1)),0)];
    end
    [accuracy,thresh]=max(sensitivity.*length(engel2(engel2==1))/length(engel2)+specificity.*(length(engel2(engel2>1))/length(engel2)));
    sensitivities=[sensitivities; sensitivity];
    specificities=[specificities; specificity];
    aurocs(b)=trapz([0 1-specificity],[0 sensitivity]);
    accuracies(b)=accuracy;
end
[prctile(aurocs,5) mean(aurocs) prctile(aurocs,95)]
[prctile(accuracies,5) mean(accuracies) prctile(accuracies,95)]

Threshes=sort(avgdists);
sensitivity=[];
specificity=[];
for i=1:length(Threshes)
    sensitivity=[sensitivity length(avgdists(Engels==1 & avgdists<=Threshes(i)))./...
        (max(length(avgdists(Engels==1)),0))];
    specificity=[specificity length(avgdists(Engels>1&  avgdists>Threshes(i)))./max(length(Threshes(Engels>1)),0)];
end
[accuracy,thresh]=max(sensitivity.*length(Engels(Engels==1))/length(Engels)+specificity.*(length(Engels(Engels>1))/length(Engels)))
aurocs=trapz([0 1-specificity],[0 sensitivity])
figure
plot(1-specificity,sensitivity)
% print(['Figures/SOZConsistencyROC.pdf'],'-dpdf','-painters')

% Plot data subdivided by Temporal lobe or not
figure
dataTL=NaN(43,4);
dataTL(Engels==1 & TL==1,1)=avgdists(Engels==1 & TL==1)';
dataTL(Engels>1 & TL==1,2)=avgdists(Engels>1 & TL==1)';
dataTL(Engels==1 & TL==0,3)=avgdists(Engels==1 & TL==0)';
dataTL(Engels>1 & TL==0,4)=avgdists(Engels>1 & TL==0)';
boxplot(dataTL)
hold on
plot(ones(1,length(data(TL==1 & Engels==1)))+rand(1,length(data(TL==1 & Engels==1))).*0.2-0.1,(data(TL==1 & Engels==1)),'ko')
plot(ones(1,length(data(TL==1 & Engels>1,2))).*2+rand(1,length(data(TL==1 & Engels>1,2))).*0.2-0.1,(data(TL==1 & Engels>1,2)),'ko')
plot(ones(1,length(data(TL==0 & Engels==1))).*3+rand(1,length(data(TL==0 & Engels==1))).*0.2-0.1,(data(TL==0 & Engels==1)),'ko')
plot(ones(1,length(data(TL==0 & Engels>1,2))).*4+rand(1,length(data(TL==0 & Engels>1,2))).*0.2-0.1,(data(TL==0 & Engels>1,2)),'ko')
anovan(full(avgdists(:)),{Engels(:)==1,TL(:)},'varnames',{'Engel','TL'})
[p,t,stats]=anovan(full(avgdists(:)),{Engels(:)==1,TL(:)},'model','interaction','varnames',{'Engel','Temporal'})
% print(['Figures/SOZConsistencyCDS_TL.pdf'],'-dpdf','-painters')
[h,p,ci,stats]=ttest2(full(avgdists(Engels==1& TL==1)),full(avgdists(Engels>1& TL==1)))
[h,p,ci,stats]=ttest2(full(avgdists(Engels==1& TL==0)),full(avgdists(Engels>1& TL==0)))

% Temporal Lobe ROC
sensitivities=[];
specificities=[];
data=avgdists(TL==1);
Engel2=Engels(TL==1);
for b=1:1000
    
    randvals=randi(length(data),length(data),1);
    threshes=data(randvals);
    engel2=Engel2(randvals);
    while length(threshes(engel2==1))==0 | length(threshes(engel2==1))==length(data)
        randvals=randi(length(data),length(data),1);
        threshes=data(randvals);
        engel2=Engel2(randvals);
    end
    threshes2=sort(threshes);
    sensitivity=[];
    specificity=[];
    for i=1:length(threshes)
        sensitivity=[sensitivity length(threshes(engel2==1 & threshes<=threshes2(i)))./...
            (max(length(threshes(engel2==1)),0))];
        specificity=[specificity length(threshes(engel2>1&  threshes>threshes2(i)))./max(length(threshes(engel2>1)),0)];
    end
    [accuracy,thresh]=max(sensitivity.*length(engel2(engel2==1))/length(engel2)+specificity.*(length(engel2(engel2>1))/length(engel2)));
    sensitivities=[sensitivities; sensitivity];
    specificities=[specificities; specificity];
    aurocs(b)=trapz([0 1-specificity],[0 sensitivity]);
    accuracies(b)=accuracy;
end
[prctile(aurocs,5) mean(aurocs) prctile(aurocs,95)]
[prctile(accuracies,5) mean(accuracies) prctile(accuracies,95)]

Threshes=sort(avgdists);
sensitivity=[];
specificity=[];
for i=1:length(Threshes)
    sensitivity=[sensitivity length(avgdists(Engels==1 & avgdists<=Threshes(i) & TL==1))./...
        (max(length(avgdists(Engels==1 & TL==1)),0))];
    specificity=[specificity length(avgdists(Engels>1 & TL==1 &  avgdists>Threshes(i)))./max(length(Threshes(Engels>1 & TL==1)),0)];
end
figure
plot(1-specificity,sensitivity)
% print(['Figures/SOZConsistencyROC_TL.pdf'],'-dpdf','-painters')
[accuracyTL,threshTL]=max(sensitivity.*length(Engels(Engels==1 & TL==1))/length(Engels(TL==1))+specificity.*(length(Engels(Engels>1 & TL==1))/length(Engels(TL==1))))
aurocsTL=trapz([0 1-specificity],[0 sensitivity])

% Extratemporal Lobe ROC
sensitivities=[];
specificities=[];
data=avgdists(TL==0);
Engel2=Engels(TL==0);
for b=1:1000
    
    randvals=randi(length(data),length(data),1);
    threshes=data(randvals);
    engel2=Engel2(randvals);
    while length(threshes(engel2==1))==0 | length(threshes(engel2==1))==length(data)
        randvals=randi(length(data),length(data),1);
        threshes=data(randvals);
        engel2=Engel2(randvals);
    end
    threshes2=sort(threshes);
    sensitivity=[];
    specificity=[];
    for i=1:length(threshes)
        sensitivity=[sensitivity length(threshes(engel2==1 & threshes<=threshes2(i)))./...
            (max(length(threshes(engel2==1)),0))];
        specificity=[specificity length(threshes(engel2>1&  threshes>threshes2(i)))./max(length(threshes(engel2>1)),0)];
    end
    [accuracy,thresh]=max(sensitivity.*length(engel2(engel2==1))/length(engel2)+specificity.*(length(engel2(engel2>1))/length(engel2)));
    sensitivities=[sensitivities; sensitivity];
    specificities=[specificities; specificity];
    aurocs(b)=trapz([0 1-specificity],[0 sensitivity]);
    accuracies(b)=accuracy;
end
[prctile(aurocs,5) mean(aurocs) prctile(aurocs,95)]
[prctile(accuracies,5) mean(accuracies) prctile(accuracies,95)]

Threshes=sort(avgdists);
sensitivity=[];
specificity=[];
for i=1:length(Threshes)
    sensitivity=[sensitivity length(avgdists(Engels==1 & avgdists<=Threshes(i) & TL==0))./...
        (max(length(avgdists(Engels==1 & TL==0)),0))];
    specificity=[specificity length(avgdists(Engels>1 & TL==0 &  avgdists>Threshes(i)))./max(length(Threshes(Engels>1 & TL==0)),0)];
end
[accuracyETL,threshETL]=max(sensitivity.*length(Engels(Engels==1 & TL==0))/length(Engels(TL==0))...
    +specificity.*(length(Engels(Engels>1 & TL==0))/length(Engels(TL==0))))
aurocsETL=trapz([0 1-specificity],[0 sensitivity])
figure
plot(1-specificity,sensitivity)
% print(['Figures/SOZConsistencyROC_ETL.pdf'],'-dpdf','-painters')
%% Consistency Necessary number seizures
AUROCS=[];
AUROCSTL=[];
AUROCSETL=[];
ACC=[];
ACCTL=[];
ACCETL=[];
for numSeizures=2:5
    aurocs=[];
    accuracies=[];
    aurocsTL=[];
    accuraciesTL=[];
    aurocsETL=[];
    accuraciesETL=[];
    for j=1:1000
        patients=[2 3 7 19 20 24 25 26 27 30 31 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
        Engels=[4 1 2 1 1 1 4 4 1 2 3 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3];
        TL=[1 0 0 0 1 1 0 0 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];
        numSpikes=[4 4 4 2 3 12 8 6 5 5 3 3 5 3 3 5 5 1 2 5 3 3 5 5 3 5 5 5 5 4 3 5 3 0 2 4 5 5 3 2 5 5 5 5 5 3];
        for jitter=1%1:9
            for trials=11
                load(['Recons/SOZ_Loc_Cutoff_' num2str(cutoffThresh(trials)) '_Jitter_' num2str(jitters(jitter)) '.mat'])
                for patientNum=1:length(patients)
                    dists=[];
                    if numSpikes(patientNum)>=numSeizures
                        %                     seizures=randperm(numSpikes(patientNum),numSeizures);
                        seizures=randperm(numSpikes(patientNum),min(numSeizures,numSpikes(patientNum)));
                        avg=mean(horzcat(AvgLoc{patientNum,seizures}),2);
                        for spikenum =seizures
                            if length(AvgLoc{patientNum,spikenum})==3
                                dists=[dists sqrt(sum((avg-AvgLoc{patientNum,spikenum}).^2))];
                            end
                        end
                    end
                    avgdists(patientNum)=mean(dists);
                end
            end
        end
        patients(isnan(avgdists))=[];
        Engels(isnan(avgdists))=[];
        TL(isnan(avgdists))=[];
        avgdists(isnan(avgdists))=[];
        
        Threshes=sort(avgdists);
        sensitivity=[];
        specificity=[];
        for i=1:length(Threshes)
            sensitivity=[sensitivity length(avgdists(Engels==1 & avgdists<=Threshes(i)))./...
                (max(length(avgdists(Engels==1)),0))];
            specificity=[specificity length(avgdists(Engels>1&  avgdists>Threshes(i)))./max(length(Threshes(Engels>1)),0)];
        end
        [accuracy,thresh]=max(sensitivity.*length(Engels(Engels==1))/length(Engels)...
            +specificity.*(length(Engels(Engels>1))/length(Engels)));
        accuracies=[accuracies accuracy];
        aurocs=[aurocs trapz([0 1-specificity],[0 sensitivity])];
        
        Threshes=sort(avgdists);
        sensitivity=[];
        specificity=[];
        for i=1:length(Threshes)
            sensitivity=[sensitivity length(avgdists(Engels==1 & avgdists<=Threshes(i) & TL==1))./...
                (max(length(avgdists(Engels==1 & TL==1)),0))];
            specificity=[specificity length(avgdists(Engels>1 & TL==1 &  avgdists>Threshes(i)))./max(length(Threshes(Engels>1 & TL==1)),0)];
        end
        [accuracyTL,threshTL]=max(sensitivity.*length(Engels(Engels==1 & TL==1))/length(Engels(TL==1))...
            +specificity.*(length(Engels(Engels>1 & TL==1))/length(Engels(TL==1))));
        accuraciesTL=[accuraciesTL accuracyTL];
        aurocsTL=[aurocsTL trapz([0 1-specificity],[0 sensitivity])];
        
        Threshes=sort(avgdists);
        sensitivity=[];
        specificity=[];
        for i=1:length(Threshes)
            sensitivity=[sensitivity length(avgdists(Engels==1 & avgdists<=Threshes(i) & TL==0))./...
                (max(length(avgdists(Engels==1 & TL==0)),0))];
            specificity=[specificity length(avgdists(Engels>1 & TL==0 &  avgdists>Threshes(i)))./max(length(Threshes(Engels>1 & TL==0)),0)];
        end
        [accuracyETL,threshETL]=max(sensitivity.*length(Engels(Engels==1 & TL==0))/length(Engels(TL==0))...
            +specificity.*(length(Engels(Engels>1 & TL==0))/length(Engels(TL==0))));
        accuraciesETL=[accuraciesETL accuracyETL];
        aurocsETL=[aurocsETL trapz([0 1-specificity],[0 sensitivity])];
        
    end
    AUROCS=[AUROCS;aurocs];
    AUROCSTL=[AUROCSTL;aurocsTL];
    AUROCSETL=[AUROCSETL;aurocsETL];
%     AUROCS=[AUROCS;[prctile(aurocs,2.5) mean(aurocs) prctile(aurocs,97.5)]];
%     ACC=[ACC;[prctile(accuracies,2.5) mean(accuracies) prctile(accuracies,97.5)]];
%     AUROCSTL=[AUROCSTL;[prctile(aurocsTL,2.5) mean(aurocsTL) prctile(aurocsTL,97.5)]];
%     ACCTL=[ACCTL;[prctile(accuraciesTL,2.5) mean(accuraciesTL) prctile(accuraciesTL,97.5)]];
%     ACCETL=[ACCETL;[prctile(accuraciesETL,2.5) mean(accuraciesETL) prctile(accuraciesETL,97.5)]];
%     AUROCSETL=[AUROCSETL;[prctile(aurocsETL,2.5) mean(aurocsETL) prctile(aurocsETL,97.5)]];
end
figure
errorbar((2:5)-0.1,AUROCS(:,2),AUROCS(:,2)-AUROCS(:,1),AUROCS(:,3)-AUROCS(:,2))
hold on
errorbar(2:5,AUROCSTL(:,2),AUROCSTL(:,2)-AUROCSTL(:,1),AUROCSTL(:,3)-AUROCSTL(:,2))
errorbar((2:5)+0.1,AUROCSETL(:,2),AUROCSETL(:,2)-AUROCSETL(:,1),AUROCSETL(:,3)-AUROCSETL(:,2))
axis([1.8 5.2 0 1])
% print('Figures/NecessaryNumSeizuresConsistencyFixed.pdf','-dpdf','-painters')
