load data/SizeSeizureData.mat
engel=[1 2 1 1 1 1 2 3 3 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 2 2 1 1 3 1 3 1 2 3 3 ];

areas = [];
for p = 1:40
    areas = [areas;mean(output.sizes{p},1)];
end

figure
plot([0 2:20],areas(engel==1,:)./100,'r')

hold on
plot([0 2:20],mean(areas(engel==1,:))./100,'g','LineWidth',3)
plot([0 2:20],areas(engel>1,:)./100,'b')
plot([0 2:20],mean(areas(engel>1,:))./100,'k','LineWidth',3)
xlabel('Source Amplitude Threshold')
ylabel('Reconstruction Area (cm^2)')

figure
boxplot(areas(engel==1,7)./100)
hold on
plot(ones(length(areas(engel==1,7)),1)+rand(length(areas(engel==1,7)),1).*0.2-0.1,areas(engel==1,7)./100,'k*')
ylabel('Reconstruction Area (cm^2)')
xlabel('Engel 1')
axis([0 2 0 650])
print -dpdf ../Figures/ReconArea_Engel1.pdf

figure
boxplot(areas(engel>1,7)./100)
hold on
plot(ones(length(areas(engel>1,7)),1)+rand(length(areas(engel>1,7)),1).*0.2-0.1,areas(engel>1,7)./100,'k*')
ylabel('Reconstruction Area (cm^2)')
xlabel('Engel 2-4')
axis([0 2 0 650])
print -dpdf ../Figures/ReconArea_Engel24.pdf

engels=repmat(engel',[1,20]);
threshes=repmat([0 2:20],[40,1]);
[p,t,stats]=anovan(areas(:),{engels(:)>1,threshes(:)},'model','interaction','varnames',{'Engel','Thresh'})
ps=[];
for i=1
    [h,p,ci,stats] = ttest2(areas(engel==1,i),areas(engel>1,i))
    ps=[ps p];
end
% output