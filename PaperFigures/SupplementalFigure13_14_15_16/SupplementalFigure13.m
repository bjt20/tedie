load data/SeizureReconData.mat
%% Duo - Supplemental Figure 13
numpatients = size(outputComp.DuoJaccard,1);
numNoises = size(outputComp.DuoJaccard,2);
jaccards=[];
coms=[];
jAll=[];
cAll=[];
for p=1:numpatients
    for j = 3%:numNoises
        jaccards=[jaccards mean(outputComp.DuoJaccard(p,j))];
        coms=[coms mean(outputComp.DuoCOM(p,j))];
%         jAll=[jAll;(outputComp.DuoJaccard{p,j}{1})];
%         cAll=[cAll;(outputComp.DuoCOM{p,j}{1})];
    end
end
patients=[2 3 7 19 20 24 25 26 27 30 31 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];

seedLocs=[15963 232085 62829 36311 227603 283019 41880 64396 266219 61819 154118 172754 230861 245733 47679 207058 52427 ...
    264581 273508 298519 98098 260907 ...
    240671 277601 245524 55646 229750 ...
    208945 297530 65342 145424 46101 ...
    263781 264220 1542 101653 83982 ...
    170071 45425 16357 132476 119192 ...
    299911 205281 27931 228919];
seedLocs2=[79662 126529 257639 42460 45929 152144 184777 162950 111124 25294 186293 259193 154670 138856 30020 3294 142533 265869 197869 ...
    232569 91991 92534 181491 169326 144380 7021 178424 64146 213729 223940 221761 160650 134606 30939 87439 ...
    124104 103313 176861 204864 119241 53703 266774 37461 242548 72912 194611];
dists=[];
for p=1:numpatients
    load(['D:\ClusterBackup\HeadModelAutomation\Patient' num2str(patients(p)) '\work/OmitCorticalPoints.mat'])
    if isfile('work/OmitNonCortex.mat')
        load('work/OmitNonCortex.mat')
        omitinds=[omitinds omits];
    end
    load(['D:\ClusterBackup\HeadModelAutomation\Patient' num2str(patients(p)) '\work/SmoothedBrainMesh.mat'])
    TRI2=scirunfield.face';
    Vertice_Location2=scirunfield.node;
    elemLoc2=(Vertice_Location2(:,TRI2(:,1))+Vertice_Location2(:,TRI2(:,2))+Vertice_Location2(:,TRI2(:,3)))./3;
    elemLoc2(:,omitinds)=[];
    dists=[dists sqrt(sum((elemLoc2(:,seedLocs(p))-elemLoc2(:,seedLocs2(p))).^2))];
end
figure
histogram(jaccards)
xlabel('Jaccard Index','FontSize',16)
ylabel('# Synthetic Source Recons','FontSize',16)
axis([0 1 0 15])
% print('../Figures/JaccardDuo.pdf','-dpdf')

figure
histogram(coms)
xlabel('COM Distance (mm)','FontSize',16)
ylabel('# Synthetic Source Recons','FontSize',16)
axis([0 50 0 16])
% print('../Figures/COMDuo.pdf','-dpdf')

figure
histogram(dists)
axis([0 120 0 15])
% print('../Figures/DistDuo.pdf','-dpdf')

figure
plot(dists,jaccards,'k*')
hold on
plot(dists([2 15 25]),jaccards([2 15 25]),'r*')
xlabel('Source to Source Distance (mm)')
ylabel('Jaccard Index')
axis([0 120 0 1])
% print('../Figures/JaccardDuoScatter.pdf','-dpdf')

figure
plot(dists,coms,'k*')
hold on
plot(dists([2 15 25]),coms([2 15 25]),'r*')
xlabel('Source to Source Distance (mm)')
ylabel('COM Distance (mm)')
axis([0 120 0 50])