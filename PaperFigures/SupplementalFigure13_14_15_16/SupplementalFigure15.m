load data/SeizureReconData.mat
%% RealTime 
numpatients = size(outputComp.RealTimeJaccard,1);
numSeizures = size(outputComp.RealTimeJaccard,2);
jaccards=zeros(numpatients,1);
coms=zeros(numpatients,1);
highs=zeros(numpatients,1);
highAmps=zeros(numpatients,1);
for p=1:numpatients
        ct=0;
        for i = 1:numSeizures
            if outputComp.RealTimeCOM(p,i)>0
                jaccards(p)=jaccards(p)+outputComp.RealTimeJaccard(p,i);
                coms(p)=coms(p)+outputComp.RealTimeCOM(p,i);
                highs(p)=highs(p)+outputComp.RealTimeHigh(p,i);
                highAmps(p)=highAmps(p)+outputComp.RealTimeHighAmp(p,i);
                ct=ct+1;
            end
        end
        if ct>0
            jaccards(p)=jaccards(p)/ct;
            coms(p)=coms(p)/ct;
            highs(p)=highs(p)/ct;
            highAmps(p)=highAmps(p)/ct;
        end
end
highAmps(jaccards==0,:)=[];
highs(jaccards==0,:)=[];
jaccards(jaccards==0,:)=[];
% coms(isnan(coms))=100;
coms(coms==0,:)=[];

figure% Use jaccard index and center of mass to make the argument that real timve vs time points does not matter
% boxplot(jaccards)
histogram(jaccards)
title('Real Time vs Time Points','FontSize',16)
xlabel('Jaccard Index','FontSize',16)
axis([0 1 0 20])
print('../Figures/JaccardRealTime.pdf','-dpdf')

figure
histogram(coms,0:1:18)
title('Real Time vs Time Points','FontSize',16)
xlabel('COM Distance (mm)','FontSize',16)
axis([0 20 0 30])
print('../Figures/COMRealTime.pdf','-dpdf')

figure
histogram(highs)
title('Real Time vs Time Points','FontSize',16)
xlabel('Highest Amplitude Pts Distance (mm)','FontSize',16)
axis([0 35 0 35])
print('../Figures/HighestAmpRealTime.pdf','-dpdf')