load data/SeizureReconData.mat
%% Reference
numpatients = size(outputComp.RereferenceJaccard,1);
numSeizures = size(outputComp.RereferenceJaccard,2);
numRefs = size(outputComp.RereferenceJaccard,3);
jaccards=[];
coms=[];
highs=[];
for p=1:numpatients
    for j = 1:numRefs
        ct=0;
        for i = 1:numSeizures
            if outputComp.RereferenceCOM(p,i,1)>0
                jaccards=[jaccards outputComp.RereferenceJaccard(p,i,j)];
                coms=[coms outputComp.RereferenceCOM(p,i,j)];
                highs=[highs outputComp.RereferenceHigh(p,i,j)];
                ct=ct+1;
            end
        end
    end
end

figure
histogram(jaccards)
hold on
plot([outputComp.RereferenceJaccard(35,1,1) outputComp.RereferenceJaccard(35,1,1)],[0 80],'r--')
plot([outputComp.RereferenceJaccard(35,1,2) outputComp.RereferenceJaccard(35,1,2)],[0 80],'m--')
plot([outputComp.RereferenceJaccard(35,1,3) outputComp.RereferenceJaccard(35,1,3)],[0 80],'b--')
xlabel('Jaccard Index','FontSize',16)
ylabel('# Seizure Comparisons','FontSize',16)
print('../Figures/JaccardRereference.pdf','-dpdf')

figure
histogram(coms)
hold on
plot([outputComp.RereferenceCOM(35,1,1) outputComp.RereferenceCOM(35,1,1)],[0 140],'r--')
plot([outputComp.RereferenceCOM(35,1,2) outputComp.RereferenceCOM(35,1,2)],[0 140],'m--')
plot([outputComp.RereferenceCOM(35,1,3) outputComp.RereferenceCOM(35,1,3)],[0 140],'b--')
xlabel('COM Distance (mm)','FontSize',16)
ylabel('# Seizure Comparisons','FontSize',16)
print('../Figures/COMRereference.pdf','-dpdf')

figure % Use this one to tell a story with 4 diff comparisons to show 5 mm vs 20 mm vs 40 mm vs 100 mm - want to say that referencing can do some stuff but in 80% of the cases it does not not change much
% need to figure out why some change so much with rereferencing.
histogram(highs)
hold on
plot([outputComp.RereferenceHigh(35,1,1) outputComp.RereferenceHigh(35,1,1)],[0 400],'r--')
plot([outputComp.RereferenceHigh(35,1,2) outputComp.RereferenceHigh(35,1,2)],[0 400],'m--')
plot([outputComp.RereferenceHigh(35,1,3) outputComp.RereferenceHigh(35,1,3)],[0 400],'b--')
xlabel('Highest Amplitude Pts Distance (mm)','FontSize',16)
ylabel('# Seizure Comparisons','FontSize',16)
print('../Figures/HighRereference.pdf','-dpdf')