load data/SeizureReconData.mat
%% Remove Elecs
numpatients = size(outputComp.RemoveElecJaccard,1);
numSeizures = size(outputComp.RemoveElecJaccard,2);
numRad = size(outputComp.RemoveElecJaccard,3);
jaccards=zeros(numpatients,numRad);
coms=zeros(numpatients,numRad);
highs=zeros(numpatients,numRad);
highAmps=zeros(numpatients,numRad);
for p=1:numpatients
    for j = 1:numRad
        ct=0;
        for i = 1:numSeizures
            if outputComp.RemoveElecCOM(p,i,1)>0
                jaccards(p,j)=jaccards(p,j)+outputComp.RemoveElecJaccard(p,i,j);
                coms(p,j)=coms(p,j)+outputComp.RemoveElecCOM(p,i,j);
                highs(p,j)=highs(p,j)+outputComp.RemoveElecHigh(p,i,j);
                highAmps(p,j)=highAmps(p,j)+outputComp.RemoveElecHighAmp(p,i,j);
                ct=ct+1;
            end
        end
        if ct>0
            jaccards(p,j)=jaccards(p,j)/ct;
            coms(p,j)=coms(p,j)/ct;
            highs(p,j)=highs(p,j)/ct;
            highAmps(p,j)=highAmps(p,j)/ct;
        end
    end
end
origHighAmps=outputComp.OrigHighAmp([1:33 35:end]);
highs(sum(jaccards,2)==0,:)=[];
highAmps(sum(jaccards,2)==0,:)=[];
jaccards(sum(jaccards,2)==0,:)=[];
% coms(isnan(coms))=100;
coms(sum(coms,2)==0,:)=[];

jaccards2=jaccards;
jaccards2(jaccards2==0)=nan;

coms2=coms;
coms2(coms2==0)=nan;

highs2=highs;
highs2(highs2==0)=nan;

figure
plot(5:5:70,jaccards2','k-')
hold on
plot(5:5:70,mean(jaccards2,'omitnan'),'r-','LineWidth',3)
plot(5,outputComp.RemoveElecJaccard(4,1,1),'bo','MarkerSize',20)
plot(20,outputComp.RemoveElecJaccard(4,1,4),'bo','MarkerSize',20)
xlabel('Removed Contact Radius (mm)','FontSize',16)
ylabel('Jaccard Index','FontSize',16)
print('../Figures/JaccardRemoveElec.pdf','-dpdf')

figure
plot(5:5:70,coms2','k-')
hold on
plot(5:5:70,mean(coms2,'omitnan'),'r-','LineWidth',3)
plot(5,outputComp.RemoveElecCOM(4,1,1),'bo','MarkerSize',20)
plot(20,outputComp.RemoveElecCOM(4,1,4),'bo','MarkerSize',20)
xlabel('Removed Contact Radius (mm)','FontSize',16)
ylabel('COM Distance (mm)','FontSize',16)
print('../Figures/comRemoveElec.pdf','-dpdf')

figure
plot(5:5:70,highs2','k-')
hold on
plot(5:5:70,mean(highs2,'omitnan'),'r-','LineWidth',3)
plot(5,outputComp.RemoveElecHigh(4,1,1),'bo','MarkerSize',20)
plot(20,outputComp.RemoveElecHigh(4,1,4),'bo','MarkerSize',20)
xlabel('Removed Contact Radius (mm)','FontSize',16)
ylabel('Highest Amplitude Pts Distance (mm)','FontSize',16)
print('../Figures/HighRemoveElec.pdf','-dpdf')