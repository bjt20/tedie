%% Creates panels for Figure 5 and Supplemental Figures 9-11
addpath ../../Code/VisFtns/
%% Synthetic source Recon - TEDIE
tic
samplingFreqs=[2048 2048 2048 2048 2048 2048 2048 2048 1024 2048 2048 2048];
propagationSpeeds=[0 0.015 0.03];% 0 for no movement, >0 for movement
noises=[6 10 20];%db
growRates=[0 0.05 0.1];

patientNum=25;
data3=[];
views=[-80,10];
patient=['Patient' num2str(patientNum)];
load(['Data/' patient '/Gradients2.mat'])%V

load(['Data/' patient '/SmoothedBrainMesh2.mat'])
TRI=scirunfield.face';
Vertice_Location=scirunfield.node;
elemLoc=(Vertice_Location(:,TRI(:,1))+Vertice_Location(:,TRI(:,2))+Vertice_Location(:,TRI(:,3)))./3;
V2=[];
temp=load(['Data/' patient '/Gradients.mat']);%V
V2=temp.V;
load(['Data/' patient '/SmoothedBrainMesh.mat'])
TRI2=scirunfield.face';
Vertice_Location2=scirunfield.node;
elemLoc2=(Vertice_Location2(:,TRI2(:,1))+Vertice_Location2(:,TRI2(:,2))+Vertice_Location2(:,TRI2(:,3)))./3;

[inds] = knnsearch(elemLoc2',elemLoc','K',1,'Distance','euclidean');


transfer=sparse(inds,1:length(TRI),ones(length(TRI),1),length(TRI2),length(TRI));
load(['Data/' patient '/OmitCorticalPoints.mat'])
load(['Data/' patient '/OmitNonCortex.mat'])
omitinds=[omitinds omits];
elemLoc2(:,omitinds)=[];
V2(:,omitinds)=[];
V2(sum(V2,2)~=0,:)=[];
transfer(omitinds,:)=[];
load(['Data/' patient '/OmitCorticalPoints2.mat'])
load(['Data/' patient '/OmitNonCortex2.mat'])
omitinds=[omitinds omits];
elemLoc(:,omitinds)=[];
V(:,omitinds)=[];
V(sum(V,2)~=0,:)=[];
transfer(:,omitinds)=[];
TRI(omitinds,:)=[];
graph1=CreateElementGraph(V,elemLoc);
graph2=CreateElementGraph(V2,elemLoc2);
noise=3;
for grow=[1 3]
    for prop=[1 3]
        %Load optimizations TEDIE
        load(['Recons\Patient' num2str(patientNum) '\J_sol_1_1_' num2str(noises(noise)) '_' ...
            num2str(propagationSpeeds(prop)*1000)...
            '_' num2str(growRates(grow)*100) '_ISO.mat'])
        %Load true sources
        load(['Sources\Patient' num2str(patientNum) '\PatchLoc_' num2str(noises(noise)) '_' ...
            num2str(propagationSpeeds(prop)*1000)...
            '_' num2str(growRates(grow)*100) '.mat'])
        % Parse Optimizations
        % Visualize
        animation_frames={};
        allVerts=cell(1000,1);
        trueVerts=sparse(1000,length(elemLoc2));
        for times=926:-100:26
            verts=[];
            if ~isempty(J_sol{times})
                for i=1:length(J_sol{times}(:,1))
                    verts=[verts makePatchOnly(J_sol{times}(i,2),graph1,J_sol{times}(i,1))];
                end
            end
            trueVerts(times,patches{times})=1;
            allVerts{times}=sparse(ones(1,length(verts)),verts,ones(1,length(verts)),1,length(elemLoc(1,:)));
            %                 allVerts{times}=verts;
        end
        % Load optimizations IRES
        load(['Recons\Patient' num2str(patientNum) '\J_sol_IRES_' num2str(noises(noise)) '_' ...
            num2str(propagationSpeeds(prop)*1000)...
            '_' num2str(growRates(grow)*100) '_alpha0p05_ISOVec.mat'])
        % Load optimizations sLORETA
        load(['Recons\Patient' num2str(patientNum) '\J_sol_SLORETA_' num2str(noises(noise)) '_' ...
            num2str(propagationSpeeds(prop)*1000)...
            '_' num2str(growRates(grow)*100) '_Thresh50_ISOVec.mat'])
        trueVertsMap=trueVerts*transfer;
        TrueVis=sparse(1,size(trueVertsMap,2));
        ReconVisTEDIE=sparse(1,size(trueVertsMap,2));
        ReconVisIRES=sparse(1,size(trueVertsMap,2));
        ReconVissLORETA=sparse(1,size(trueVertsMap,2));
        for times=926:-100:26%51:100:950
            %     data=sum(vertcat(allVerts{(times-25):(times+25)}));
            %     data(data<20)=0;
            data=allVerts{times};
            data=data>0;
            ReconVisTEDIE(data)=times;
            data=abs(J_sol{times,1})>(max(abs(J_sol{times,1})).*0.1);
            ReconVisIRES(data)=times;
            data=LIM{times}>0;
            ReconVissLORETA(data)=times;
            %     data2=sum(trueVertsMap((times-25):(times+25),:))>0;
            data2=trueVertsMap(times,:)>0;
            TrueVis(data2)=times;
        end
        figure
        plotBrainSourceSingle(full(TrueVis),TRI,Vertice_Location,views)
%                 print(['Figures/GroundTruthSynthetic_' num2str(grow) '_' num2str(prop) '.png'],'-dpng')
        figure
        plotBrainSourceSingle(full(ReconVisTEDIE),TRI,Vertice_Location,views)
%                 print(['Figures/TEDIESynthetic_' num2str(grow) '_' num2str(prop) '.png'],'-dpng')
        figure
        plotBrainSourceSingle(full(ReconVisIRES),TRI,Vertice_Location,views)
%                 print(['Figures/IRESSynthetic_' num2str(grow) '_' num2str(prop) '.png'],'-dpng')
        figure
        plotBrainSourceSingle(full(ReconVissLORETA),TRI,Vertice_Location,views)
%                 print(['Figures/sLORETASynthetic_' num2str(grow) '_' num2str(prop) '.png'],'-dpng')
    end
end
toc
%% Synthetic Source Algorithm Comparison Violin Plots
patients=[2 3 7 19 20 24 25 26 27 30 31 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
% grow,prop,noise,patientNum,times,AlgoType
growRates=[0 0.05 0.1];
propagationSpeeds=[0 0.015 0.03];% 0 for no movement, >0 for movement
noises=[6 10 20];%db
ct=1;

noiseLevels=[];
            propagationLevels=[];
            growlevels=[];
            modelTypes=[];
            jaccardsFinal=[];
            mappedsFinal=[];
            distsFinal=[];
for i=1:3
    for j=1:3
        for k=1:3
            load(['Recons/AllMetrics_' num2str(i) '_' num2str(j) '_' num2str(k) '.mat'],'jaccardsFinal','mappedsFinal','distsFinal')
            %% Jaccard
            figure
            colors=copper(4);
            % colors=repmat(colors,9,1);
            data=reshape(jaccardsFinal,4,length(jaccardsFinal)/4)';
            boxplot(data)
            hold on
            plot([ones(1,46)+rand(1,46).*0.2-0.1 ones(1,46).*2+rand(1,46).*0.2-0.1 ones(1,46).*3+rand(1,46).*0.2-0.1 ones(1,46).*4+rand(1,46).*0.2-0.1],data(:),'ko')
            
            
            xticks([1 2 3 4])
            xticklabels({'TEDIE','sLORETA','IRES','IE'});
            a = get(gca,'XTickLabel');
            set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
            ylabel('Jaccard Index','FontSize',20)
            axis([0.5 4.5 0 1])
            
            % set(gcf,'outerposition',[1 1 1880 1080])
            fileName=['Figures\JaccardBoxSyntheticSourcesAllAlgos_npg_' num2str(i) num2str(j) num2str(k) '_All4.pdf'];
%             print(gcf,fileName,'-bestfit','-dpdf')
            
            %% % mapped
            figure
            colors=copper(4);
            % colors=repmat(colors,9,1);
            data=reshape(mappedsFinal,4,length(mappedsFinal)/4)';
            boxplot(data.*100)
            hold on
            plot([ones(1,46)+rand(1,46).*0.2-0.1 ones(1,46).*2+rand(1,46).*0.2-0.1 ones(1,46).*3+rand(1,46).*0.2-0.1 ones(1,46).*4+rand(1,46).*0.2-0.1],data(:).*100,'ko')
            
            
            xticks([1 2 3 4])
            xticklabels({'TEDIE','sLORETA','IRES','IE'});
            a = get(gca,'XTickLabel');
            set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
            ylabel('% Mapped','FontSize',20)
            axis([0.5 4.5 0 100])
            
            fileName=['Figures\MappedBoxSyntheticSourcesAllAlgos_npg_' num2str(i) num2str(j) num2str(k) '_All4.pdf'];
%             print(gcf,fileName,'-bestfit','-dpdf')
            %% Distance
            figure
            colors=copper(4);
            data=reshape(distsFinal,4,length(distsFinal)/4)';
            
            boxplot(data)
            hold on
            plot([ones(1,46)+rand(1,46).*0.2-0.1 ones(1,46).*2+rand(1,46).*0.2-0.1 ones(1,46).*3+rand(1,46).*0.2-0.1 ones(1,46).*4+rand(1,46).*0.2-0.1],data(:),'ko')
             
            xticks([1 2 3 4])
            xticklabels({'TEDIE','sLORETA','IRES','IE'});
            a = get(gca,'XTickLabel');
            set(gca,'XTickLabel',a,'fontsize',12,'FontWeight','bold')
            ylabel('Center of Mass Location Error (mm)','FontSize',20)
            axis([0.5 4.5 0 25])
            
            fileName=['Figures\DistBoxSyntheticSourcesAllAlgos_npg_' num2str(i) num2str(j) num2str(k) '_All4.pdf'];
%             print(gcf,fileName,'-bestfit','-dpdf')
        end
    end
end