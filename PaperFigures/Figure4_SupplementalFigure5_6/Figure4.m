% Code to generate boxplots in Figure 4 and Supplemental Figures 5-6
addpath('../../Code/VisFtns/')
%% Plot data
load('Data\MTLvATL_50Thresh_Windows.mat') % vary Window for fixed threshold of 50 from 2:2:10 s - Add _RestrictednATL to get data for Supplemental Figure 6
% load('Data\MTLvATL_Threshes_6sWindow.mat') % vary Threshold for fixed - Add _RestrictednATL to get data for Supplemental Figure 6
% window size of 6 s from [25 50 75 99]
ATLSourcewhich = ATLSource{3}; % Select which to plot
figure
boxplot([ATLSourcewhich(Treatment==1)' [ATLSourcewhich(Treatment==2)';nan;nan;nan] [ATLSourcewhich(Treatment==3)';nan;nan;nan]].*100,'Color','k')
hold on
plot(ones(9,1)+(rand(9,1)-0.5).*0.2,ATLSourcewhich(Treatment==1).*100,'ko')
plot(ones(6,1).*2+(rand(6,1)-0.5).*0.2,ATLSourcewhich(Treatment==2).*100,'ko')
plot(ones(6,1).*3+(rand(6,1)-0.5).*0.2,ATLSourcewhich(Treatment==3).*100,'ko')
axis([0 4 0 100])
xticks(1:3)
xticklabels({'Engel 1 MTL Ablation','Engel 2-4 MTL Ablation','Engel 1 ATL Resection'})
ylabel('% of Seizures With ATL Source')
% print('Figures/ATLvsMTLBoxplotWSuperior_Threshold_4.pdf','-dpdf','-painters')

%% ROC
figure
ATLSourcewhich = ATLSource{2}; % Select which to plot
sortedATLSource=sort(ATLSourcewhich);
sensitivity=0;
specificity=1;
for i=1:length(sortedATLSource)
    sensitivity=[sensitivity length(ATLSourcewhich(Treatment==1 & ATLSourcewhich<=sortedATLSource(i)))./...
                (length(ATLSourcewhich(Treatment==1)))];
    specificity=[specificity length(ATLSourcewhich(ATLSourcewhich(Treatment==2)>sortedATLSource(i)))./length(ATLSourcewhich(Treatment==2))];
end
plot(1-specificity,sensitivity)
% print('Figures\MTLROC.pdf','-dpdf')