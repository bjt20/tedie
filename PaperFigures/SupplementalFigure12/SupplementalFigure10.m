%% Code to make Supplemental Figure 10
load Data\OtherAlgorithmJitter.mat 
patients=[3 7 19 20 24 27 30 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
engel=[1 2 1 1 1 1 2 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3 ];
TL=[0 0 0 1 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];
jitters=[-1 1 -2 2 -5 5 -10 10 -20 20 -50 50 -100 100 -200 200 -500 500 -1000 1000];
TL([11 30])=[];
engel([11 30])=[];
tot=[];
engel2=[];
TL2=[];
totsLORETA=[];
for i=[1:10 12:29 31:length(patients)]
    temp=mindistsBestOrig(i,:);
    temp(temp==-1)=[];
    tot=[tot median(temp)];
end
threshes=sort(tot);

sensitivity=[];
specificity=[];
for i=1:length(threshes)
    sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
        (length(tot(engel==1)))];
    specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
end
accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
aurocs=trapz(1-specificity,sensitivity);
accuracies=max(accuracy);


totsLORETA=[totsLORETA;tot];
for jitter=1:length(jitters)
    tot=[];
    engel2=[];
    TL2=[];
    for i=[1:10 12:29 31:length(patients)]
        temp=mindistsBest(i,:,jitter);
        temp(temp==-1)=[];
        tot=[tot median(temp)];
    end
    threshes=sort(tot);
    
    sensitivity=[];
    specificity=[];
    for i=1:length(threshes)
        sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
            (length(tot(engel==1)))];
        specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
    end
    accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
    aurocs=[aurocs trapz(1-specificity,sensitivity)];
    accuracies=[accuracies max(accuracy)];

%     totsLORETA=[totsLORETA;tot];
%     threshes=sort(tot);
end
figure
plot([0 jitters],aurocs,'ko')
hold on
plot([0],aurocs(1),'ro')
axis([-1000 1000 0 1])
xlabel('# Time Points From Peak')
ylabel('AUROC')
% print('Figures/SLORETAJitter.pdf','-dpdf','-painters')
%%
patients=[3 7 19 20 24 27 30 33 112,116,117,130,132,134,135,138,139,140,141,144,146,148,150,151,157,158,160,162,164,165,166,171,172,173,177,179,180,181,185,187,188,190];
engel=[1 2 1 1 1 1 2 3 3 1 1 1 3 1 2 4 1 1 1 1 1 1 1 2 1 3 1 3 1 3 3 2 2 1 1 3 1 3 1 2 3 3 ];
TL=[0 0 0 1 1 1 1 0 0 1 1 0 1 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 0 1 1 0 0 1 1 1 0 1];
jitters=[-1 1 -2 2 -5 5 -10 10 -20 20 -50 50 -100 100 -200 200 -500 500 -1000 1000];
TL([11 30])=[];
engel([11 30])=[];
tot=[];
engel2=[];
TL2=[];
totsLORETA=[];
for i=[1:10 12:29 31:length(patients)]
    temp=mindistsBestIRESOrig(i,:);
    temp(temp==-1)=[];
    tot=[tot median(temp)];
end
threshes=sort(tot);

sensitivity=[];
specificity=[];
for i=1:length(threshes)
    sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
        (length(tot(engel==1)))];
    specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
end
accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
aurocs=trapz(1-specificity,sensitivity);
accuracies=max(accuracy);


totsLORETA=[totsLORETA;tot];
for jitter=1:length(jitters)
    tot=[];
    engel2=[];
    TL2=[];
    for i=[1:10 12:29 31:length(patients)]
        temp=mindistsBestIRES(i,:,jitter);
        temp(temp==-1)=[];
        tot=[tot median(temp)];
    end
    threshes=sort(tot);
    
    sensitivity=[];
    specificity=[];
    for i=1:length(threshes)
        sensitivity=[sensitivity length(tot(engel==1 & tot<=threshes(i)))./...
            (length(tot(engel==1)))];
        specificity=[specificity length(tot(engel>1 & tot>threshes(i)))./length(tot(engel>1))];
    end
    accuracy=sensitivity.*length(engel(engel==1))/length(engel)+specificity.*(length(engel(engel>1))/length(engel));
    aurocs=[aurocs trapz(1-specificity,sensitivity)];
    accuracies=[accuracies max(accuracy)];

%     totsLORETA=[totsLORETA;tot];
%     threshes=sort(tot);
end
figure
plot([0 jitters],aurocs,'ko')
hold on
plot([0],aurocs(1),'ro')
axis([-1000 1000 0 1])
xlabel('# Time Points From Peak')
ylabel('AUROC')
% print('Figures/IRESJitter.pdf','-dpdf','-painters')